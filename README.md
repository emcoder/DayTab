# DayTab

**DayTab** - flexible day planner, smart timetable application.  

**Tech stack:**

- TypeScript
- Node.js
- GraphQL
- React

**To Do:**

- Configurable day start/end time (night can be a day part)
