import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('users', (table) => {
    table.increments('id');
    table.string('username').notNullable().unique({ indexName: 'users_username-unique' });
    table.string('password').notNullable();
    table.string('salt').notNullable();
    table.string('time_zone');
    table.string('language').notNullable();
    table.timestamp('created_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('updated_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('deleted_at', { useTz: true, precision: 3 }).nullable();

    table.index('username', 'users_username-index');
  });

  await knex.schema.createTable('tasks', (table) => {
    table.increments('id');
    table.string('name').notNullable();
    table.timestamp('start', { useTz: true, precision: 3 }).notNullable();
    table.timestamp('end', { useTz: true, precision: 3 }).notNullable();
    table.integer('user_id').notNullable();
    table.timestamp('completed_at', { useTz: true, precision: 3 }).nullable();
    table.timestamp('created_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('updated_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('deleted_at', { useTz: true, precision: 3 }).nullable();

    table.index('start', 'tasks-start-index');
    table.index('user_id', 'tasks-user_id-index');

    table.foreign('user_id', 'tasks-user_id-fk')
      .references('id')
      .inTable('users')
      .onDelete('RESTRICT')
      .onUpdate('CASCADE');

    table.check('?? < ??', ['start', 'end']);
  });

  await knex.schema.createTable('days', (table) => {
    table.increments('id');
    table.integer('user_id').notNullable();
    table.date('date').notNullable();
    table.time('start_time').notNullable();
    table.time('end_time').notNullable();
    table.timestamp('created_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('updated_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    
    table.unique(['user_id', 'date'], {
      indexName: 'days-user_id-date-index',
    })

    table.foreign('user_id', 'days-user_id-fk')
      .references('id')
      .inTable('users')
      .onDelete('RESTRICT')
      .onUpdate('CASCADE');
  });

  await knex.schema.createTable('tg_users', (table) => {
    table.increments('id');
    table.integer('user_id').notNullable();
    table.integer('chat_id').notNullable();
    table.timestamp('created_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('updated_at', { useTz: true, precision: 3 })
      .notNullable()
      .defaultTo(knex.fn.now(3));
    table.timestamp('deleted_at', { useTz: true, precision: 3 }).nullable();

    table.foreign('user_id', 'tg_users-user_id-fk')
      .references('id')
      .inTable('users')
      .onDelete('RESTRICT')
      .onUpdate('CASCADE');
    table.unique(['user_id', 'chat_id'], {
      indexName: 'tg_users-user_id-chat_id-unique_index',
    });
  });
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('days');
  await knex.schema.dropTable('tasks');
  await knex.schema.dropTable('tg_users');
  await knex.schema.dropTable('users');
}

