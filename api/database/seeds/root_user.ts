import { Knex } from 'knex';
import { createSaltAndHash, createRandomString } from '../../src/common/auth';

async function createRandomPassword() {
  return await createRandomString(20);
}

export async function seed(knex: Knex): Promise<void> {
  const password = process.env.ROOT_PASSWORD || await createRandomPassword();

  const { salt, hash } = await createSaltAndHash(password);

  const tz = Intl.DateTimeFormat().resolvedOptions().timeZone || 'UTC';
  
  await knex('users').insert({
    username: 'root',
    password: hash,
    salt,
    time_zone: tz,
    language: 'en',
  });

  console.log('Your "root" user created!');
  console.log(`Your "root" user time zone was set to ${tz}`);
  console.log('*** ATTENTION ***');
  console.log(`Your "root" user password: ${password}`);
  console.log('*** REMEMBER IT ***');
};
