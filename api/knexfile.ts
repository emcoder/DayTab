import type { Knex } from "knex";
import * as dotenv from 'dotenv'

dotenv.config();

// Update with your config settings.

const config: { [key: string]: Knex.Config } = {
  development: {
    client: 'postgresql',
    connection: {
      host: process.env.DB_HOST || '127.0.0.1',
      port: Number(process.env.DB_PORT) || 5432,
      user: process.env.DB_USER || 'daytab',
      password: process.env.DB_PASSWORD || '1234567',
      database: process.env.DB_NAME || 'daytab',
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations',
      directory: 'database/migrations',
    },
    seeds: {
      directory: 'database/seeds',
    },
  }
};

module.exports = config;
