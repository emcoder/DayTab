import { AuthModule } from '@core/auth';
import { Module } from '@nestjs/common';
import { AuthResolver } from './auth.resolver';

@Module({
  imports: [AuthModule],
  providers: [AuthResolver],
})
export class AuthGraphQLModule {}
