import { AuthService } from '@core/auth/auth.service';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { AuthResult } from './auth.models';

@Resolver('Auth')
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => AuthResult)
  async signIn(@Args('username') username: string, @Args('password') password: string) {
    return this.authService.signIn(username, password);
  }
}
