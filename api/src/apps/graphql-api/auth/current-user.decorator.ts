import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthUser, GraphQLRequestWithUser } from './types';

export const CurrentUser = createParamDecorator(
  (_data: unknown, context: ExecutionContext): AuthUser => {
    const ctx = GqlExecutionContext.create(context);
    const { user } = ctx.getContext<{ req: GraphQLRequestWithUser }>().req;
    return user;
  },
);
