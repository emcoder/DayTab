import { UserId } from '@daytab/common';
import { FastifyRequest } from 'fastify';

export interface AuthUser {
  id: UserId;
}

export type GraphQLRequestWithUser = FastifyRequest & { user: AuthUser };
