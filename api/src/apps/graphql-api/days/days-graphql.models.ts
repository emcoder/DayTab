import { DayDate, Time, UserId } from '@daytab/common';
import { Field, GraphQLISODateTime, Int, ObjectType } from '@nestjs/graphql';
import { GraphQLString } from 'graphql';

@ObjectType()
export class Day {
  @Field(() => Int)
  id!: number;

  @Field(() => GraphQLString)
  date!: DayDate;

  @Field(() => GraphQLString)
  startTime!: Time;

  @Field(() => GraphQLString)
  endTime!: Time;

  @Field(() => Int)
  userId!: UserId;

  @Field(() => GraphQLISODateTime)
  createdAt!: Date;

  @Field(() => GraphQLISODateTime)
  updatedAt!: Date;
}
