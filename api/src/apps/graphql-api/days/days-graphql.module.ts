import { DaysModule } from '@core/days';
import { UsersModule } from '@core/users';
import { Module } from '@nestjs/common';
import { DaysMutations } from './days-graphql.mutations';
import { DaysQueries } from './days-graphql.queries';

@Module({
  providers: [DaysQueries, DaysMutations],
  imports: [DaysModule, UsersModule],
})
export class DaysGraphQLModule {}
