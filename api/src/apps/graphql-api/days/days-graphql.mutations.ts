import { DaysService } from '@core/days';
import { DayDate, Time, UserId } from '@daytab/common';
import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { GraphQLInt, GraphQLString } from 'graphql';
import { GqlAuthGuard } from '../auth';
import { Day } from './days-graphql.models';

@Resolver(Day)
@UseGuards(GqlAuthGuard)
export class DaysMutations {
  constructor(private readonly daysService: DaysService) {}

  @Mutation(() => Day, { name: 'saveDay' })
  async save(
    @Args('date', { type: () => GraphQLString }) date: DayDate,
    @Args('startTime', { type: () => GraphQLString }) startTime: Time,
    @Args('endTime', { type: () => GraphQLString }) endTime: Time,
    @Args('userId', { type: () => GraphQLInt }) userId: UserId,
  ) {
    return this.daysService.save({
      date,
      startTime,
      endTime,
      userId,
    });
  }
}
