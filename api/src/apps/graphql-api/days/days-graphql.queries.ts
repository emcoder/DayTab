import { DaysService } from '@core/days';
import { DayDate, UserId } from '@daytab/common';
import { UseGuards } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { GraphQLInt, GraphQLString } from 'graphql';
import { GqlAuthGuard } from '../auth';
import { Day } from './days-graphql.models';

@Resolver(Day)
@UseGuards(GqlAuthGuard)
export class DaysQueries {
  constructor(private readonly daysService: DaysService) {}

  @Query(() => [Day])
  async days(@Args('userId', { type: () => GraphQLInt }) userId: UserId) {
    return this.daysService.findAll({
      userId,
    });
  }

  @Query(() => Day, { nullable: true })
  async day(
    @Args('userId', { type: () => GraphQLInt }) userId: UserId,
    @Args('date', { type: () => GraphQLString }) date: DayDate,
  ) {
    return this.daysService.findOne({
      userId,
      date,
    });
  }
}
