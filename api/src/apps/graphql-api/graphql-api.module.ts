import { TasksGraphQLModule } from './tasks';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MercuriusDriver, MercuriusDriverConfig } from '@nestjs/mercurius';
import { join } from 'path';
import { LoggerModule } from 'nestjs-pino';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { KnexModule } from 'nest-knexjs';
import { UsersGraphQLModule } from './users';
import { AuthGraphQLModule } from './auth/auth-graphql.module';
import { DaysGraphQLModule } from './days';
import { rabbitMqConfig } from '@frameworks/rabbitmq';
import { databaseConfig } from '@frameworks/database';
import { EventEmitterModule } from '@nestjs/event-emitter';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [rabbitMqConfig, databaseConfig],
      cache: true,
    }),
    GraphQLModule.forRoot<MercuriusDriverConfig>({
      path: '/graphql',
      driver: MercuriusDriver,
      graphiql: true,
      autoSchemaFile: join(process.cwd(), 'assets/schema.gql'),
    }),
    LoggerModule.forRoot({
      pinoHttp: {
        name: 'GraphQL API',
        level: process.env.LOG_LEVEL || (process.env.NODE_ENV !== 'production' ? 'trace' : 'info'),
        transport: process.env.NODE_ENV !== 'production' ? { target: 'pino-pretty' } : undefined,
      },
    }),
    KnexModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        config: {
          client: 'pg',
          connection: {
            host: configService.get<string>('database.host'),
            port: configService.get<number>('database.port'),
            user: configService.get<string>('database.user'),
            password: configService.get<string>('database.password'),
            database: configService.get<string>('database.name'),
          },
        },
      }),
    }),
    EventEmitterModule.forRoot(),
    TasksGraphQLModule,
    UsersGraphQLModule,
    AuthGraphQLModule,
    DaysGraphQLModule,
  ],
})
export class GraphQLApiModule {}
