import { Logger } from 'nestjs-pino';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import { GraphQLApiModule } from './graphql-api.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(GraphQLApiModule, new FastifyAdapter());
  app.useLogger(app.get(Logger));
  const configService = app.get(ConfigService);
  await app.listen(
    Number(configService.get('GRAPHQL_API_PORT')) || 3000,
    configService.get<string>('GRAPHQL_API_HOST') || 'localhost',
  );
}

bootstrap();
