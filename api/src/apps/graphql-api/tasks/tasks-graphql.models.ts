import { UserId } from '@daytab/common';
import { Field, GraphQLISODateTime, Int, ObjectType } from '@nestjs/graphql';
import { User } from '../users';

@ObjectType()
export class Task {
  @Field(() => Int)
  id!: number;

  @Field()
  name!: string;

  @Field(() => GraphQLISODateTime)
  start!: Date;

  @Field(() => GraphQLISODateTime)
  end!: Date;

  @Field(() => Int)
  userId!: UserId;

  @Field(() => GraphQLISODateTime)
  createdAt!: Date;

  @Field(() => GraphQLISODateTime)
  updatedAt!: Date;

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt!: Date | null;

  @Field(() => User)
  user!: User;
}
