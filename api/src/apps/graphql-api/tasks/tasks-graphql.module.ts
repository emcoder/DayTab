import { TasksModule } from '@core/tasks';
import { UsersModule } from '@core/users';
import { Module } from '@nestjs/common';
import { TasksResolver } from './tasks-graphql.resolver';

@Module({
  providers: [TasksResolver],
  imports: [TasksModule, UsersModule],
})
export class TasksGraphQLModule {}
