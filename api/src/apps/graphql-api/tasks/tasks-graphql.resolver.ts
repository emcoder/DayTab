import { TasksService, UpdateTaskParams } from '@core/tasks';
import { UsersService } from '@core/users';
import { UseGuards } from '@nestjs/common';
import {
  Args,
  GraphQLISODateTime,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { AuthUser, CurrentUser, GqlAuthGuard } from '../auth';
import { User } from '../users';
import { Task } from './tasks-graphql.models';

@Resolver(Task)
@UseGuards(GqlAuthGuard)
export class TasksResolver {
  constructor(
    private readonly tasksService: TasksService,
    private readonly usersService: UsersService,
  ) {}

  @Query(() => [Task])
  async tasks(
    @Args('start', { type: () => GraphQLISODateTime, nullable: true }) start?: Date | null,
    @Args('end', { type: () => GraphQLISODateTime, nullable: true }) end?: Date | null,
  ) {
    return this.tasksService.findAll({
      start,
      end,
    });
  }

  @ResolveField(() => User)
  async user(@Parent() task: Task) {
    const { userId } = task;
    return this.usersService.findOne(userId);
  }

  @Query(() => Task)
  async task(@Args('id', { type: () => Int }) id: number) {
    return this.tasksService.findOne(id);
  }

  @Mutation(() => Task)
  async createTask(
    @Args('name', { type: () => String }) name: string,
    @Args('start', { type: () => GraphQLISODateTime }) start: Date,
    @Args('end', { type: () => GraphQLISODateTime }) end: Date,
    @CurrentUser() user: AuthUser,
  ) {
    return this.tasksService.create({
      name,
      start,
      end,
      userId: user.id,
    });
  }

  @Mutation(() => Task)
  async updateTask(
    @Args('id', { type: () => Int }) id: number,
    @Args('name', { type: () => String, nullable: true }) name?: string | null,
    @Args('start', { type: () => GraphQLISODateTime, nullable: true }) start?: Date | null,
    @Args('end', { type: () => GraphQLISODateTime, nullable: true }) end?: Date | null,
  ) {
    const data: UpdateTaskParams = {};
    if (name) {
      data.name = name;
    }
    if (start) {
      data.start = start;
    }
    if (end) {
      data.end = end;
    }

    return this.tasksService.update(id, data);
  }

  @Mutation(() => Task)
  async deleteTask(@Args('id', { type: () => Int }) id: number) {
    return this.tasksService.remove(id);
  }
}
