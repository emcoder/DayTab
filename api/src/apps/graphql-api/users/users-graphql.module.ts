import { AuthModule } from '@core/auth';
import { UsersModule } from '@core/users';
import { Module } from '@nestjs/common';
import { UsersResolver } from './users-graphql.resolver';

@Module({
  imports: [UsersModule, AuthModule],
  providers: [UsersResolver],
})
export class UsersGraphQLModule {}
