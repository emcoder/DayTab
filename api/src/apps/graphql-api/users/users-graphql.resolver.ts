import { ParseBrandPipe } from '@common/pipes';
import { AuthService } from '@core/auth/auth.service';
import { UsersService, User as CoreUser } from '@core/users';
import { TgChatId, TgChatIdType, TimeZone, UserId } from '@daytab/common';
import { UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { GraphQLInt } from 'graphql';
import { AuthUser, CurrentUser, GqlAuthGuard } from '../auth';
import { User } from './users-graphql.models';

@Resolver()
@UseGuards(GqlAuthGuard)
export class UsersResolver {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @Query(() => [User])
  async users() {
    return this.usersService.findAll();
  }

  @Query(() => User)
  async user(
    @Args('id', { type: () => Int, nullable: true }) id: UserId,
    @CurrentUser() user: CoreUser,
  ) {
    if (!id && user) {
      return this.usersService.findOne(user.id);
    }
    return null;
  }

  @Mutation(() => User)
  async createUser(
    @Args('username') username: string,
    @Args('password') password: string,
    @Args('timeZone') timeZone: TimeZone,
    @Args('language') language: string,
  ) {
    return this.usersService.create({
      username,
      password,
      timeZone,
      language,
    });
  }

  @Mutation(() => User)
  async updateUser(
    @Args('username') username: string,
    @Args('password') password: string,
    @Args('timeZone') timeZone: TimeZone,
    @Args('language') language: string,
    @CurrentUser() user: AuthUser,
  ) {
    return this.usersService.update(user.id, {
      username,
      password,
      timeZone,
      language,
    });
  }

  @Mutation(() => GraphQLInt)
  async userLinkToTelegram(
    @Args(
      {
        name: 'tgChatId',
        type: () => GraphQLInt,
      },
      new ParseBrandPipe(TgChatIdType),
    )
    chatId: TgChatId,
    @CurrentUser() user: AuthUser,
  ) {
    await this.usersService.linkToTelegram(user.id, chatId);
    return chatId;
  }
}
