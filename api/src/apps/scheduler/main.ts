import { getAmqpUrl, RabbitMqQueue } from '@daytab/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import { SchedulerModule } from './scheduler.module';

async function bootstrap() {
  const app = await NestFactory.create(SchedulerModule, new FastifyAdapter());

  const configService = app.get<ConfigService>(ConfigService);

  app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [
        getAmqpUrl({
          host: configService.getOrThrow('rabbitmq.host'),
          port: configService.getOrThrow('rabbitmq.port'),
          user: configService.getOrThrow('rabbitmq.user'),
          password: configService.getOrThrow('rabbitmq.password'),
        }),
      ],
      queue: RabbitMqQueue.SchedulerTasks,
      queueOptions: {
        durable: true,
      },
    },
  });
  await app.startAllMicroservices();
}
bootstrap();
