import { Module } from '@nestjs/common';
import { LoggerModule } from 'nestjs-pino';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { KnexModule } from 'nest-knexjs';
import { rabbitMqConfig } from '@frameworks/rabbitmq';
import { databaseConfig } from '@frameworks/database';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksSchedulerModule } from './tasks';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      load: [rabbitMqConfig, databaseConfig],
      cache: true,
    }),
    LoggerModule.forRoot({
      pinoHttp: {
        name: 'Scheduler',
        level: process.env.LOG_LEVEL || (process.env.NODE_ENV !== 'production' ? 'debug' : 'info'),
        transport: process.env.NODE_ENV !== 'production' ? { target: 'pino-pretty' } : undefined,
      },
    }),
    KnexModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        config: {
          client: 'pg',
          connection: {
            host: configService.get<string>('database.host'),
            port: configService.get<number>('database.port'),
            user: configService.get<string>('database.user'),
            password: configService.get<string>('database.password'),
            database: configService.get<string>('database.name'),
          },
        },
      }),
    }),
    TasksSchedulerModule,
  ],
})
export class SchedulerModule {}
