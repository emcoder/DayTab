import { TaskEventType } from '@daytab/common';
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { TasksScheduler } from './tasks.scheduler';

@Controller()
export class TasksSchedulerController {
  constructor(private readonly tasksScheduler: TasksScheduler) {}

  @EventPattern(TaskEventType.Created)
  async handleTaskCreated() {
    await this.tasksScheduler.initSchedule();
  }

  @EventPattern(TaskEventType.Updated)
  async handleTaskUpdated() {
    await this.tasksScheduler.initSchedule();
  }

  @EventPattern(TaskEventType.Deleted)
  async handleTaskDeleted() {
    await this.tasksScheduler.initSchedule();
  }
}
