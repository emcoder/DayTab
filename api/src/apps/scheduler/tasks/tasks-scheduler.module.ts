import { TasksModule } from '@core/tasks';
import { MessageBrokerModule } from '@frameworks/message-broker';
import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksSchedulerController } from './tasks-scheduler.controller';
import { TasksScheduler } from './tasks.scheduler';

@Module({
  controllers: [TasksSchedulerController],
  imports: [TasksModule, ScheduleModule, MessageBrokerModule],
  providers: [TasksScheduler],
})
export class TasksSchedulerModule {}
