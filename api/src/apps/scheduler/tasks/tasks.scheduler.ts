import { Task, TasksService } from '@core/tasks';
import { TaskEndedEvent, TaskStartedEvent } from '@daytab/common';
import { MessageBrokerService } from '@frameworks/message-broker';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';
import { Logger } from 'nestjs-pino';

@Injectable()
export class TasksScheduler implements OnModuleInit {
  constructor(
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly logger: Logger,
    private readonly tasksService: TasksService,
    private readonly messageBroker: MessageBrokerService,
  ) {}

  async onModuleInit() {
    await this.initSchedule();
  }

  async initSchedule() {
    this.logger.log('Tasks scheduler initialization...');

    this.clearJobs();

    const tasks = await this.tasksService.findForScheduler();

    for (const task of tasks) {
      this.scheduleTaskJobs(task);
    }
  }

  private clearJobs() {
    const jobs = this.schedulerRegistry.getCronJobs();
    for (const [name, job] of jobs) {
      job.stop();
      this.schedulerRegistry.deleteCronJob(name);
    }
  }

  private scheduleTaskStartedJob(task: Task) {
    const { id, start } = task;
    const logTaskIdentifier = `ID = ${id}, start = ${start.toISOString()}`;

    this.logger.debug(`Schedule task (${logTaskIdentifier}) start notification`);
    const jobKey = `task-${id}-start`;
    const job = new CronJob({
      cronTime: start,
      onTick: () => {
        this.messageBroker.emitTasksEvent(new TaskStartedEvent(task.toDto()));
        this.schedulerRegistry.deleteCronJob(jobKey);
      },
    });
    this.schedulerRegistry.addCronJob(jobKey, job);
    job.start();
  }

  private scheduleTaskEndedJob(task: Task) {
    const { id, end } = task;
    const logTaskIdentifier = `ID = ${id}, end = ${end.toISOString()}`;

    this.logger.debug(`Schedule task (${logTaskIdentifier}) end notification`);
    const jobKey = `task-${id}-end`;
    const job = new CronJob({
      cronTime: end,
      onTick: () => {
        this.messageBroker.emitTasksEvent(new TaskEndedEvent(task.toDto()));
        this.schedulerRegistry.deleteCronJob(jobKey);
      },
    });
    this.schedulerRegistry.addCronJob(jobKey, job);
    job.start();
  }

  private scheduleTaskJobs(task: Task) {
    const now = new Date();

    const { start, end } = task;

    if (start > now) {
      this.scheduleTaskStartedJob(task);
    }

    if (end > now) {
      this.scheduleTaskEndedJob(task);
    }
  }
}
