import { PasswordHash, PasswordSalt } from '@core/users/types';
import { randomBytes, scrypt } from 'crypto';

import { SALT_SIZE, SCRYPT_KEY_SIZE } from './constants';

export function createRandomString(len: number): Promise<string> {
  const bytesNum = Math.ceil(len / 2);
  return new Promise((resolve, reject) => {
    randomBytes(bytesNum, (err, buffer) => {
      if (err) {
        return reject(err);
      }

      return resolve(buffer.toString('hex').substring(0, len));
    });
  });
}

export async function createSalt(size: number = SALT_SIZE): Promise<PasswordSalt> {
  return (await createRandomString(size)) as PasswordSalt;
}

export function createHash(password: string, salt: PasswordSalt): Promise<PasswordHash> {
  const bytesNum = Math.floor(SCRYPT_KEY_SIZE / 2);
  return new Promise((resolve, reject) => {
    scrypt(password, salt, bytesNum, (err, key) => {
      if (err) {
        reject(err);
      }
      resolve(<PasswordHash>key.toString('hex'));
    });
  });
}

export async function createSaltAndHash(
  password: string,
): Promise<{ hash: PasswordHash; salt: PasswordSalt }> {
  const salt = await createSalt();
  const hash = await createHash(password, salt);
  return {
    hash,
    salt,
  };
}
