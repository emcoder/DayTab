import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import { isLeft } from 'fp-ts/Either';
import * as t from 'io-ts';

@Injectable()
export class ParseBrandPipe<B extends t.Any, T> implements PipeTransform {
  constructor(private readonly brand: t.BrandC<B, T>) {}

  transform(value: T, _metadata: ArgumentMetadata): t.Branded<t.TypeOf<B>, T> {
    const result = this.brand.decode(value);
    if (isLeft(result)) {
      throw new Error(result.left.map((err) => err.message).join('; '));
    }
    const parsed: t.Branded<t.TypeOf<B>, T> = result.right;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return parsed;
  }
}
