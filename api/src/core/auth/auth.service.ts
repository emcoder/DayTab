import { createHash } from '@common/auth';
import { User } from '@core/users';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { AuthResult } from './auth.types';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<User | null> {
    const user = await this.usersService.findByUsername(username);
    if (!user) {
      return null;
    }

    const { salt, password: expectedHash } = user;

    const hash = await createHash(password, salt);
    if (hash === expectedHash) {
      return user;
    }

    return null;
  }

  async signIn(username: string, password: string): Promise<AuthResult> {
    const user = await this.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException('Authorization failed');
    }

    return {
      token: this.jwtService.sign(user),
    };
  }
}
