import { DayDate, Time, UserId } from '@daytab/common';

export class Day {
  id!: number;

  userId!: UserId;

  date!: DayDate;

  startTime!: Time;

  endTime!: Time;

  createdAt!: Date;

  updatedAt!: Date;
}

export type FindAllDaysParams = Pick<Day, 'userId'>;
export type FindOneDayParams = Pick<Day, 'id'> | Pick<Day, 'userId' | 'date'>;
export type SaveDayParams = Pick<Day, 'userId' | 'date' | 'startTime' | 'endTime'>;
