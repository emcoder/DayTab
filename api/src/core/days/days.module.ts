import { DaysRepositoryModule } from '@api/frameworks/storage/days-repository';
import { Module } from '@nestjs/common';
import { DaysService } from './days.service';

@Module({
  imports: [DaysRepositoryModule],
  providers: [DaysService],
  exports: [DaysService],
})
export class DaysModule {}
