import { DaysRepositoryService } from '@api/frameworks/storage/days-repository';
import { Injectable } from '@nestjs/common';
import { FindAllDaysParams, FindOneDayParams, SaveDayParams } from './days.models';

@Injectable()
export class DaysService {
  constructor(private readonly daysRepository: DaysRepositoryService) {}

  async findAll(params: FindAllDaysParams) {
    return this.daysRepository.findAll(params);
  }

  async findOne(params: FindOneDayParams) {
    const day = await this.daysRepository.findOne(params);
    return day;
  }

  async save(params: SaveDayParams) {
    const result = await this.daysRepository.save(params);
    return result;
  }
}
