import { UserId, TaskId, TaskDto, IsoDate } from '@daytab/common';

export class Task {
  id!: TaskId;

  name!: string;

  start!: Date;

  end!: Date;

  userId!: UserId;

  createdAt!: Date;

  updatedAt!: Date;

  deletedAt!: Date | null;

  toDto(): TaskDto {
    const { id, name, start, end, userId } = this;

    return {
      id,
      name,
      start: <IsoDate>start.toISOString(),
      end: <IsoDate>end.toISOString(),
      userId,
    };
  }
}

export class CreateTaskParams {
  name!: string;

  start!: Date;

  end!: Date;

  userId!: UserId;
}

export class UpdateTaskParams {
  name?: string;

  start?: Date;

  end?: Date;
}

export class FindAllTasksParams {
  start?: Date | null | CompareCondition<Date>;

  end?: Date | null;

  userId?: number | number[] | null;
}

export type CompareOperator = '>' | '<' | '=' | '>=' | '<=';

export interface CompareCondition<T> {
  operator: CompareOperator;
  value: T;
}
