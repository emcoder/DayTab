import { TasksRepositoryModule } from '@api/frameworks/storage/tasks-repository';
import { MessageBrokerModule } from '@frameworks/message-broker';
import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';

@Module({
  imports: [TasksRepositoryModule, MessageBrokerModule],
  providers: [TasksService],
  exports: [TasksService],
})
export class TasksModule {}
