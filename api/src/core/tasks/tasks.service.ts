import { TasksRepositoryService } from '@api/frameworks/storage/tasks-repository/tasks-repository.service';
import { Injectable } from '@nestjs/common';
import { TaskCreatedEvent, TaskDeletedEvent, TaskUpdatedEvent } from '@daytab/common';
import { CreateTaskParams, FindAllTasksParams, Task, UpdateTaskParams } from './tasks.models';
import { MessageBrokerService } from '@frameworks/message-broker';

@Injectable()
export class TasksService {
  constructor(
    private readonly tasksRepository: TasksRepositoryService,
    private readonly messageBroker: MessageBrokerService,
  ) {}

  async findAll(params: FindAllTasksParams): Promise<Task[]> {
    return this.tasksRepository.findAll(params);
  }

  async findForScheduler(): Promise<Task[]> {
    return this.tasksRepository.findForScheduler();
  }

  async findOne(id: number): Promise<Task | null> {
    return this.tasksRepository.findOne(id);
  }

  async create(params: CreateTaskParams): Promise<Task> {
    const result = await this.tasksRepository.create(params);
    await this.messageBroker.emitTasksEvent(new TaskCreatedEvent(result.toDto()));
    return result;
  }

  async update(id: number, params: UpdateTaskParams): Promise<Task> {
    const result = await this.tasksRepository.update(id, params);
    await this.messageBroker.emitTasksEvent(new TaskUpdatedEvent(result.toDto()));
    return result;
  }

  async remove(id: number): Promise<Task | null> {
    const result = await this.tasksRepository.remove(id);
    if (!result) {
      return null;
    }
    await this.messageBroker.emitTasksEvent(new TaskDeletedEvent(result.toDto()));
    return result;
  }
}
