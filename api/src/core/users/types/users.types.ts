import * as t from 'io-ts';

export interface PasswordHashBrand {
  readonly PasswordHash: unique symbol;
}

export const PasswordHashType = t.brand(
  t.string,
  (value): value is t.Branded<string, PasswordHashBrand> => true,
  'PasswordHash',
);

export type PasswordHash = t.TypeOf<typeof PasswordHashType>;

export interface PasswordSaltBrand {
  readonly PasswordSalt: unique symbol;
}

export const PasswordSaltType = t.brand(
  t.string,
  (value): value is t.Branded<string, PasswordSaltBrand> => true,
  'PasswordSalt',
);

export type PasswordSalt = t.TypeOf<typeof PasswordSaltType>;
