import { TimeZone, UserId } from '@daytab/common';
import { PasswordHash, PasswordSalt } from './types';

export class User {
  id!: UserId;

  username!: string;

  password!: PasswordHash;

  salt!: PasswordSalt;

  timeZone!: TimeZone | null;

  language!: string;

  createdAt!: Date;

  updatedAt!: Date;

  deletedAt!: Date | null;
}

export class CreateUserParams {
  username!: string;

  password!: string;

  timeZone!: TimeZone | null;

  language!: string;
}

export class UpdateUserParams {
  username?: string;

  password?: string;

  timeZone?: TimeZone | null;

  language?: string;
}
