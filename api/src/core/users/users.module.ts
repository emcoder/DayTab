import { MessageBrokerModule } from '@frameworks/message-broker';
import { UsersRepositoryModule } from '@frameworks/storage/users-repository';
import { Module } from '@nestjs/common';
import { UsersService } from './users.service';

@Module({
  imports: [UsersRepositoryModule, MessageBrokerModule],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
