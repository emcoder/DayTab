import { createSaltAndHash } from '@common/auth';
import { TgChatId, UserId, UserLinkedToTelegramEvent } from '@daytab/common';
import { MessageBrokerService } from '@frameworks/message-broker';
import { UsersRepositoryService } from '@frameworks/storage/users-repository';
import { UpdateDbUserParams } from '@frameworks/storage/users-repository/types';
import { Injectable } from '@nestjs/common';
import { CreateUserParams, UpdateUserParams, User } from './users.models';

@Injectable()
export class UsersService {
  constructor(
    private readonly usersRepository: UsersRepositoryService,
    private readonly messageBroker: MessageBrokerService,
  ) {}

  async findAll() {
    return this.usersRepository.findAll();
  }

  async findOne(id: UserId): Promise<User | null> {
    return this.usersRepository.findOne(id);
  }

  async findByUsername(username: string): Promise<User | null> {
    return this.usersRepository.findByUsername(username);
  }

  async create(params: CreateUserParams) {
    const { username, password, timeZone, language } = params;
    const { salt, hash } = await createSaltAndHash(password);

    return this.usersRepository.create({
      username,
      salt,
      password: hash,
      timeZone,
      language,
    });
  }

  async update(id: UserId, params: UpdateUserParams) {
    const { password, ...data } = params;

    const updateData: UpdateDbUserParams = { ...data };
    if (password) {
      const { salt, hash } = await createSaltAndHash(password);
      updateData.salt = salt;
      updateData.password = hash;
    }

    return this.usersRepository.update(id, updateData);
  }

  async linkToTelegram(userId: UserId, tgChatId: TgChatId) {
    await this.messageBroker.emit(
      new UserLinkedToTelegramEvent({
        userId,
        tgChatId,
      }),
    );
  }
}
