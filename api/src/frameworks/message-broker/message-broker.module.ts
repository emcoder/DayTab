import { getAmqpUrl, RabbitMqQueue } from '@daytab/common';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MessageBrokerService } from './message-broker.service';
import { ClientToken } from './types';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: ClientToken.TasksEvents,
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [
              getAmqpUrl({
                host: configService.getOrThrow<string>('rabbitmq.host'),
                port: configService.getOrThrow<number>('rabbitmq.port'),
                user: configService.getOrThrow<string>('rabbitmq.user'),
                password: configService.getOrThrow<string>('rabbitmq.password'),
              }),
            ],
            queue: RabbitMqQueue.Tasks,
            queueOptions: {
              durable: true,
            },
          },
        }),
      },
      {
        name: ClientToken.SchedulerEvents,
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [
              getAmqpUrl({
                host: configService.getOrThrow<string>('rabbitmq.host'),
                port: configService.getOrThrow<number>('rabbitmq.port'),
                user: configService.getOrThrow<string>('rabbitmq.user'),
                password: configService.getOrThrow<string>('rabbitmq.password'),
              }),
            ],
            queue: RabbitMqQueue.SchedulerTasks,
            queueOptions: {
              durable: true,
            },
          },
        }),
      },
    ]),
  ],
  providers: [MessageBrokerService],
  exports: [MessageBrokerService],
})
export class MessageBrokerModule {}
