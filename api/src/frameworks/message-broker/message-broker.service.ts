import { BaseEvent, TaskEventType, UserEventType } from '@daytab/common';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { ClientToken } from './types';

@Injectable()
export class MessageBrokerService {
  constructor(
    @Inject(ClientToken.TasksEvents)
    private readonly telegramBotClient: ClientProxy,
    @Inject(ClientToken.SchedulerEvents)
    private readonly schedulerClient: ClientProxy,
  ) {}

  async emitTasksEvent(event: BaseEvent<unknown>) {
    const { type, payload } = event;

    if (
      type === TaskEventType.Created ||
      type === TaskEventType.Updated ||
      type === TaskEventType.Deleted
    ) {
      await lastValueFrom(this.schedulerClient.emit(type, payload));
    } else if (type === TaskEventType.Started || type === TaskEventType.Ended) {
      await lastValueFrom(this.telegramBotClient.emit(type, payload));
    }
  }

  async emit(event: BaseEvent<unknown>) {
    const { type, payload } = event;

    if (type === UserEventType.LinkedToTelegram) {
      await lastValueFrom(this.telegramBotClient.emit(type, payload));
    }
  }
}
