export enum ClientToken {
  TasksEvents = 'TASKS_EVENTS',
  SchedulerEvents = 'SCHEDULER_EVENTS',
}
