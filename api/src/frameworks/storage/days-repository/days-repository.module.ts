import { Module } from '@nestjs/common';
import { KnexModule } from 'nest-knexjs';
import { DaysRepositoryService } from './days-repository.service';

@Module({
  imports: [KnexModule],
  providers: [DaysRepositoryService],
  exports: [DaysRepositoryService],
})
export class DaysRepositoryModule {}
