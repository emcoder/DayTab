import { FindAllDaysParams, Day, FindOneDayParams, SaveDayParams } from '@api/core/days';
import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectConnection } from 'nest-knexjs';
import { DayRow } from './types';

@Injectable()
export class DaysRepositoryService {
  private readonly entityColumns;

  constructor(@InjectConnection() private readonly knex: Knex) {
    this.entityColumns = [
      'id',
      'user_id',
      this.knex.raw(`TO_CHAR(date, 'YYYY-MM-DD') AS date`),
      this.knex.raw(`TO_CHAR(start_time, 'HH24:MM') AS start_time`),
      this.knex.raw(`TO_CHAR(end_time, 'HH24:MI') AS end_time`),
      'created_at',
      'updated_at',
    ];
  }

  async findAll(params: FindAllDaysParams) {
    const { userId } = params;

    const query = this.knex<DayRow>('days')
      .select('*')
      .where('user_id', userId)
      .orderBy('date', 'DESC');

    const rows = await query;

    const days = rows.map((row) => this.toDto(row));
    return days;
  }

  async findOne(params: FindOneDayParams): Promise<Day | null> {
    const query = this.knex.from('days').first<DayRow | null>(this.entityColumns);

    if ('id' in params) {
      const { id } = params;

      query.where('id', id);
    } else {
      const { userId, date } = params;

      query.where('user_id', userId).where('date', date);
    }

    const result = await query;
    if (!result) {
      return null;
    }

    return this.toDto(result);
  }

  async save(day: SaveDayParams): Promise<Day> {
    const { userId, date, startTime, endTime } = day;

    const existingDay = await this.knex('days')
      .first<Pick<DayRow, 'id'> | null>('id')
      .where('user_id', userId)
      .andWhere('date', date);

    if (existingDay) {
      const { id } = existingDay;

      const [result] = await this.knex<DayRow>('days')
        .update({
          start_time: startTime,
          end_time: endTime,
        })
        .where('id', id)
        .returning<DayRow[]>(this.entityColumns);

      return this.toDto(result);
    }

    const [result] = await this.knex<DayRow>('days')
      .insert({
        user_id: userId,
        date,
        start_time: startTime,
        end_time: endTime,
      })
      .returning<DayRow[]>(this.entityColumns);

    return this.toDto(result);
  }

  private toDto(row: DayRow): Day {
    const {
      id,
      user_id: userId,
      date,
      start_time: startTime,
      end_time: endTime,
      created_at: createdAt,
      updated_at: updatedAt,
    } = row;

    return {
      id,
      userId,
      date,
      startTime,
      endTime,
      createdAt,
      updatedAt,
    };
  }
}
