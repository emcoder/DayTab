import { DayId, Time, UserId, DayDate } from '@daytab/common';

export interface DayRow {
  id: DayId;
  user_id: UserId;
  date: DayDate;
  start_time: Time;
  end_time: Time;
  created_at: Date;
  updated_at: Date;
}
