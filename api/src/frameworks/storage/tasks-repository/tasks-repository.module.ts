import { Module } from '@nestjs/common';
import { KnexModule } from 'nest-knexjs';
import { TasksRepositoryService } from './tasks-repository.service';

@Module({
  imports: [KnexModule],
  providers: [TasksRepositoryService],
  exports: [TasksRepositoryService],
})
export class TasksRepositoryModule {}
