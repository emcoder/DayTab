import { CreateTaskParams, FindAllTasksParams, Task, UpdateTaskParams } from '@api/core/tasks';
import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectConnection } from 'nest-knexjs';
import { TaskRow } from './types';

@Injectable()
export class TasksRepositoryService {
  constructor(@InjectConnection() private readonly knex: Knex) {}

  async findForScheduler(): Promise<Task[]> {
    const today = new Date();

    const query = this.knex<TaskRow>('tasks')
      .select('*')
      .where((qb) => {
        qb.where('start', '>=', today).orWhere('end', '>=', today);
      })
      .whereNull('deleted_at')
      .orderBy('start');

    const rows = await query;

    const tasks = rows.map((row) => this.getTask(row));

    return tasks;
  }

  async findAll(params: FindAllTasksParams) {
    const { start, end, userId } = params;

    const query = this.knex<TaskRow>('tasks').select('*');

    if (userId) {
      const userIds = Array.isArray(userId) ? userId : [userId];
      query.whereIn('user_id', userIds);
    }

    if (start && 'operator' in start) {
      const { operator, value } = start;
      query.where('start', operator, value);
    }

    if (start && end) {
      query.where((qb) => {
        qb.where((startQb) => {
          startQb.where('start', '>=', start).andWhere('start', '<', end);
        }).orWhere((endQb) => {
          endQb.where('end', '>', start).andWhere('end', '<=', end);
        });
      });
    }

    query.whereNull('deleted_at');

    query.orderBy('start');

    const rows = await query;

    const tasks = rows.map((row) => this.getTask(row));

    return tasks;
  }

  async findOne(id: number): Promise<Task | null> {
    const result = await this.knex
      .from('tasks')
      .first<TaskRow | null>('*')
      .where('id', id)
      .whereNull('deleted_at');
    if (!result) {
      return null;
    }
    return this.getTask(result);
  }

  async create(task: CreateTaskParams): Promise<Task> {
    const { name, start, end, userId } = task;

    const [result] = await this.knex<TaskRow>('tasks')
      .insert({
        name,
        start,
        end,
        user_id: userId,
      })
      .returning('*');

    return this.getTask(result);
  }

  async update(id: number, data: UpdateTaskParams): Promise<Task> {
    const [result] = await this.knex<TaskRow>('tasks').update(data).where('id', id).returning('*');

    return this.getTask(result);
  }

  async remove(id: number): Promise<Task | null> {
    const [result] = await this.knex<TaskRow>('tasks')
      .update('deleted_at', new Date())
      .where('id', id)
      .returning('*');

    return result ? this.getTask(result) : null;
  }

  private getTask(row: TaskRow): Task {
    const {
      id,
      name,
      start,
      end,
      user_id: userId,
      created_at: createdAt,
      updated_at: updatedAt,
      deleted_at: deletedAt,
    } = row;

    const task = new Task();

    Object.assign(task, {
      id,
      name,
      start,
      end,
      userId,
      createdAt,
      updatedAt,
      deletedAt,
    });

    return task;
  }
}
