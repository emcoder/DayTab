import { TaskId, UserId } from '@daytab/common';

export interface TaskRow {
  id: TaskId;
  name: string;
  start: Date;
  end: Date;
  user_id: UserId;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
}
