import { PasswordHash, PasswordSalt } from '@core/users/types';
import { TgChatId, TimeZone, UserId } from '@daytab/common';

export interface DbUser {
  id: UserId;
  username: string;
  password: PasswordHash;
  salt: PasswordSalt;
  time_zone: TimeZone | null;
  language: string;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date | null;
}

export interface CreateDbUserParams {
  username: string;
  password: PasswordHash;
  salt: PasswordSalt;
  timeZone: TimeZone | null;
  language: string;
}

export interface UpdateDbUserParams {
  username?: string;
  password?: PasswordHash;
  salt?: PasswordSalt;
  timeZone?: TimeZone | null;
  language?: string;
}

export interface TgUser {
  user_id: UserId;
  chat_id: TgChatId;
}
