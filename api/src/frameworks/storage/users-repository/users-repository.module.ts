import { Module } from '@nestjs/common';
import { KnexModule } from 'nest-knexjs';
import { UsersRepositoryService } from './users-repository.service';

@Module({
  imports: [KnexModule],
  providers: [UsersRepositoryService],
  exports: [UsersRepositoryService],
})
export class UsersRepositoryModule {}
