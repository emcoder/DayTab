import { User } from '@core/users';
import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectConnection } from 'nest-knexjs';
import { CreateDbUserParams, DbUser, TgUser, UpdateDbUserParams } from './types';
import { TgChatId, UserId } from '@daytab/common';

@Injectable()
export class UsersRepositoryService {
  constructor(@InjectConnection() private readonly knex: Knex) {}

  async findChatId(userId: UserId): Promise<TgChatId | null> {
    const tgUser = await this.knex<TgUser>('tg_users').first(['chat_id']).where('user_id', userId);
    if (tgUser) {
      return tgUser.chat_id;
    }

    return null;
  }

  async findAll() {
    const query = this.knex<DbUser>('users').select('*');

    query.orderBy('createdAt', 'DESC');

    const rows = await query;
    return rows.map((row) => this.getDto(row));
  }

  async findOne(id: UserId): Promise<User | null> {
    const result = await this.knex.from('users').first<DbUser | null>('*').where('id', id);
    if (!result) {
      return null;
    }
    return this.getDto(result);
  }

  async findByUsername(username: string): Promise<User | null> {
    const result = await this.knex
      .from('users')
      .first<DbUser | null>('*')
      .where('username', username);
    if (!result) {
      return null;
    }
    return this.getDto(result);
  }

  async create(user: CreateDbUserParams): Promise<User> {
    const { username, password, salt, timeZone, language } = user;

    const [result] = await this.knex<DbUser>('users')
      .insert({
        username,
        password,
        salt,
        time_zone: timeZone,
        language,
      })
      .returning('*');

    return this.getDto(result);
  }

  async update(id: UserId, data: UpdateDbUserParams): Promise<User> {
    const [result] = await this.knex<DbUser>('users').update(data).where('id', id).returning('*');

    return this.getDto(result);
  }

  private getDto(row: DbUser): User {
    const {
      id,
      username,
      password,
      salt,
      time_zone: timeZone,
      language,
      created_at: createdAt,
      updated_at: updatedAt,
      deleted_at: deletedAt,
    } = row;

    return {
      id,
      username,
      password,
      salt,
      timeZone,
      language,
      createdAt,
      updatedAt,
      deletedAt,
    };
  }
}
