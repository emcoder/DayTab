export interface BaseEvent<T> {
  type: string;
  payload: T;
}
