import { parseISO } from 'date-fns';
import * as t from 'io-ts';

export interface DayDateBrand {
  readonly DayDate: unique symbol;
}

export const isDayDate = (dayDate: string): dayDate is t.Branded<string, DayDateBrand> => {
  return parseISO(dayDate).toString() !== 'Invalid Date';
};

export const DayDateType = t.brand(t.string, isDayDate, 'DayDate');

export type DayDate = t.TypeOf<typeof DayDateType>;
