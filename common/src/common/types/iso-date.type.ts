import { parseISO } from 'date-fns';
import * as t from 'io-ts';

interface IsoDateBrand {
  readonly IsoDate: unique symbol;
}

function isIsoDate(isoDate: string): isoDate is t.Branded<string, IsoDateBrand> {
  return parseISO(isoDate).toString() !== 'Invalid Date';
}

export const IsoDateType = t.brand(t.string, isIsoDate, 'IsoDate');

export type IsoDate = t.TypeOf<typeof IsoDateType>;
