import * as t from 'io-ts';
import { PositiveType } from './positive.type';

export const PositiveIntType = t.intersection([t.Int, PositiveType]);
export type PositiveInt = t.TypeOf<typeof PositiveIntType>;
