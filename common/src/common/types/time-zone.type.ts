import * as t from 'io-ts';

interface TimeZoneBrand {
  readonly TimeZone: unique symbol;
}

export const TimeZoneType = t.brand(
  t.string,
  (value: string): value is t.Branded<string, TimeZoneBrand> =>
    typeof value === 'string' && value.length > 0,
  'TimeZone',
);

export type TimeZone = t.TypeOf<typeof TimeZoneType>;
