import * as t from 'io-ts';

interface TimeBrand {
  readonly Time: unique symbol;
}

const isTime = (time: string): time is t.Branded<string, TimeBrand> => {
  const [hours, minutes] = time.split(':');
  const hoursNum = Number(hours);
  const minutesNum = Number(minutes);
  return (
    Number.isInteger(hoursNum) &&
    Number.isInteger(minutesNum) &&
    hoursNum >= 0 &&
    hoursNum <= 23 &&
    minutesNum >= 0 &&
    minutesNum <= 59
  );
};

export const TimeType = t.brand(t.string, isTime, 'Time');

export type Time = t.TypeOf<typeof TimeType>;
