import * as t from 'io-ts';
import { PositiveInt, PositiveIntType } from '../../common/types';

export interface DayIdBrand {
  readonly DayId: unique symbol;
}

export const isDayId = (id: number): id is t.Branded<PositiveInt, DayIdBrand> => {
  return PositiveIntType.is(id);
};

export const DayIdType = t.brand(PositiveIntType, isDayId, 'DayId');

export type DayId = t.TypeOf<typeof DayIdType>;
