export function getAmqpUrl(params: {
  host: string;
  port: number;
  user: string;
  password: string;
}): string {
  const { host, port, user, password } = params;

  return `amqp://${user}:${password}@${host}:${port}`;
}
