export enum RabbitMqQueue {
  Tasks = 'tasks_queue',
  SchedulerTasks = 'scheduler_queue',
}
