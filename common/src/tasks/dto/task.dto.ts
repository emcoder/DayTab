import { IsoDate } from '../../common';
import { UserId } from '../../users';
import { TaskId } from '../types';

export interface TaskDto {
  id: TaskId;
  name: string;
  start: IsoDate;
  end: IsoDate;
  userId: UserId;
}
