import { BaseEvent } from '../../common';
import { TaskDto } from '../dto';
import { TaskEventType } from './task-event-type';

export class TaskCreatedEvent implements BaseEvent<TaskDto> {
  readonly type = TaskEventType.Created;

  constructor(readonly payload: TaskDto) {}
}
