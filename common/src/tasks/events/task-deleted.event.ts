import { BaseEvent } from '../../common';
import { TaskDto } from '../dto';
import { TaskEventType } from './task-event-type';

export class TaskDeletedEvent implements BaseEvent<TaskDto> {
  readonly type = TaskEventType.Deleted;

  constructor(readonly payload: TaskDto) {}
}
