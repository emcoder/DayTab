import { BaseEvent } from '../../common';
import { TaskDto } from '../dto';
import { TaskEventType } from './task-event-type';

export class TaskEndedEvent implements BaseEvent<TaskDto> {
  readonly type = TaskEventType.Ended;

  constructor(readonly payload: TaskDto) {}
}
