export enum TaskEventType {
  Created = 'task:created',
  Updated = 'task:updated',
  Deleted = 'task:deleted',
  Started = 'task:started',
  Ended = 'task:ended',
}
