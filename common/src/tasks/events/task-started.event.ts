import { BaseEvent } from '../../common';
import { TaskDto } from '../dto';
import { TaskEventType } from './task-event-type';

export class TaskStartedEvent implements BaseEvent<TaskDto> {
  readonly type = TaskEventType.Started;

  constructor(readonly payload: TaskDto) {}
}
