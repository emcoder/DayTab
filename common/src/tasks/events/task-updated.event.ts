import { BaseEvent } from '../../common';
import { TaskDto } from '../dto';
import { TaskEventType } from './task-event-type';

export class TaskUpdatedEvent implements BaseEvent<TaskDto> {
  readonly type = TaskEventType.Updated;

  constructor(readonly payload: TaskDto) {}
}
