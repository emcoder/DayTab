import * as t from 'io-ts';
import { PositiveInt, PositiveIntType } from '../../common';

export interface TaskIdBrand {
  readonly TaskId: unique symbol;
}

export const isTaskId = (id: number): id is t.Branded<PositiveInt, TaskIdBrand> => {
  return PositiveIntType.is(id);
};

export const TaskIdType = t.brand(PositiveIntType, isTaskId, 'TaskId');

export type TaskId = t.TypeOf<typeof TaskIdType>;
