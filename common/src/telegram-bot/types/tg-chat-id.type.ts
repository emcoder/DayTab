import * as t from 'io-ts';
import { PositiveInt, PositiveIntType } from '../../common';

export interface TgChatIdBrand {
  readonly TgChatId: unique symbol;
}

export const isTgChatId = (id: number): id is t.Branded<PositiveInt, TgChatIdBrand> => {
  return PositiveIntType.is(id);
};

export const TgChatIdType = t.brand(PositiveIntType, isTgChatId, 'TgChatId');

export type TgChatId = t.TypeOf<typeof TgChatIdType>;
