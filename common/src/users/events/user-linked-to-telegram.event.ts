import { BaseEvent } from '../../common';
import { TgChatId } from '../../telegram-bot';
import { UserId } from '../types';
import { UserEventType } from './user-event-type';

export interface UserLinkedToTelegramEventPayload {
  userId: UserId;
  tgChatId: TgChatId;
}

export class UserLinkedToTelegramEvent implements BaseEvent<UserLinkedToTelegramEventPayload> {
  readonly type = UserEventType.LinkedToTelegram;

  constructor(readonly payload: UserLinkedToTelegramEventPayload) {}
}
