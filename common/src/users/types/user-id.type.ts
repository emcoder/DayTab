import * as t from 'io-ts';
import { PositiveInt, PositiveIntType } from '../../common';

export interface UserIdBrand {
  readonly UserId: unique symbol;
}

export const isUserId = (id: number): id is t.Branded<PositiveInt, UserIdBrand> => {
  return PositiveIntType.is(id);
};

export const UserIdType = t.brand(PositiveIntType, isUserId, 'UserId');

export type UserId = t.TypeOf<typeof UserIdType>;
