module.exports = {
  apps : [
    {
      name: "graphql-api",
      cwd: "api",
      script : "npm run start:dev",
    },
    {
      name: "scheduler",
      cwd: "api",
      script : "npm run scheduler:start:dev",
    },
    {
      name: "telegram-bot",
      cwd: "telegram-bot",
      script : "npm run start:dev",
    },
    {
      name: "ui",
      cwd: "ui",
      script: "npm run start:dev",
    },
  ]
}
