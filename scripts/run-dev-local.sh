#!/usr/bin/env bash

GRAPHQL_API_DEBUG_PORT=9229
SCHEDULER_DEBUG_PORT=9230
TELEGRAM_BOT_DEBUG_PORT=9231

watch_common() (
  cd common && npm run watch
)

run_api() (
  cd api && npm run start:dev -- $GRAPHQL_API_DEBUG_PORT
)

run_scheduler() (
  cd api && npm run scheduler:start:dev -- $SCHEDULER_DEBUG_PORT
)

run_telegram_bot() (
  cd telegram-bot && npm run start:dev -- $TELEGRAM_BOT_DEBUG_PORT
)

run_ui() (
  cd ui && npm run start:dev
)

watch_common &
PID1=$!
run_api &
PID2=$!
run_scheduler &
PID3=$!
run_telegram_bot &
PID4=$!
run_ui &
PID5=$!
wait $PID1 $PID2 $PID3 $PID4