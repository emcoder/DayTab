import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SlonikModule } from 'nestjs-slonik';
import { BotModule } from './bot';
import { databaseConfig } from './frameworks/database';
import { rabbitMqConfig } from './frameworks/rabbitmq';
import { TasksModule } from './tasks';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig, rabbitMqConfig],
    }),
    TasksModule,
    BotModule,
    SlonikModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const user = configService.getOrThrow<string>('database.user');
        const password = configService.getOrThrow<string>('database.password');
        const host = configService.getOrThrow<string>('database.host');
        const port = configService.getOrThrow<string>('database.port');
        const name = configService.getOrThrow<string>('database.name');

        return {
          connectionUri: `postgres://${user}:${password}@${host}:${port}/${name}`,
        };
      },
    }),
  ],
})
export class AppModule {}
