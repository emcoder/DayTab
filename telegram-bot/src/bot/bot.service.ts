import { Injectable, OnApplicationShutdown } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Bot } from 'grammy';

@Injectable()
export class BotService implements OnApplicationShutdown {
  private readonly bot: Bot;

  private readonly appUrl: string;

  constructor(private readonly configService: ConfigService) {
    this.bot = new Bot(this.configService.getOrThrow('BOT_TOKEN'));
    this.appUrl = this.configService.getOrThrow('APP_URL');

    this.bot.command('start', (ctx) => {
      const chatId = ctx.chat.id;
      const link = this.appUrl + `/user/link-telegram/${chatId}`;
      ctx.reply(
        `Your chat ID: \`${chatId}\`\n` +
          `Follow the link to connect your DayTab account to Telegram: ${link}`,
      );
    });

    this.bot.start();
  }

  async send(params: { chatId: number; message: string }) {
    const { chatId, message } = params;
    await this.bot.api.sendMessage(chatId, message);
  }

  onApplicationShutdown() {
    this.bot.stop();
  }
}
