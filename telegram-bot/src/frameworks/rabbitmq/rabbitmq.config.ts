import { registerAs } from '@nestjs/config';

export const rabbitMqConfig = registerAs('rabbitmq', () => ({
  host: process.env.RABBITMQ_HOST || 'localhost',
  port: Number(process.env.RABBITMQ_PORT) || 5432,
  user: process.env.RABBITMQ_USER || 'guest',
  password: process.env.RABBITMQ_PASSWORD || 'guest',
}));
