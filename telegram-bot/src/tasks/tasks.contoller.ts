import { TaskEndedEvent, TaskEventType, TaskStartedEvent } from '@daytab/common';
import { Controller } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { TasksService } from './tasks.service';

@Controller()
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @EventPattern(TaskEventType.Started)
  async handleStarted(@Payload() payload: TaskStartedEvent['payload']) {
    await this.tasksService.handleStarted(payload);
  }

  @EventPattern(TaskEventType.Ended)
  async handleEnded(@Payload() payload: TaskEndedEvent['payload']) {
    await this.tasksService.handleEnded(payload);
  }
}
