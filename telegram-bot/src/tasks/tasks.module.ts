import { Module } from '@nestjs/common';
import { UsersModule } from '../users';
import { BotModule } from '../bot';
import { TasksController } from './tasks.contoller';
import { TasksService } from './tasks.service';

@Module({
  imports: [BotModule, UsersModule],
  controllers: [TasksController],
  providers: [TasksService],
})
export class TasksModule {}
