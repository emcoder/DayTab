import { TaskDto } from '@daytab/common';
import { Injectable } from '@nestjs/common';
import { format, parseISO } from 'date-fns';
import { BotService } from '../bot';
import { UsersService } from '../users';

@Injectable()
export class TasksService {
  constructor(
    private readonly botService: BotService,
    private readonly usersService: UsersService,
  ) {}

  async handleStarted(task: TaskDto) {
    const { name, start, userId } = task;
    const chatIds = await this.usersService.findChatIds(userId);
    if (chatIds.length === 0) {
      return;
    }

    const time = format(parseISO(start), 'HH:mm');

    for (const chatId of chatIds) {
      await this.botService.send({
        chatId,
        message: `Task "${name}" starts at ${time}!`,
      });
    }
  }

  async handleEnded(task: TaskDto) {
    const { name, end, userId } = task;
    const chatIds = await this.usersService.findChatIds(userId);
    if (chatIds.length === 0) {
      return;
    }

    const time = format(parseISO(end), 'HH:mm');

    for (const chatId of chatIds) {
      await this.botService.send({
        chatId,
        message: `Task "${name}" ends at ${time}!`,
      });
    }
  }
}
