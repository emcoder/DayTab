import { UserEventType, UserLinkedToTelegramEvent } from '@daytab/common';
import { Controller } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { UsersService } from './users.service';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @EventPattern(UserEventType.LinkedToTelegram)
  async handleLink(@Payload() payload: UserLinkedToTelegramEvent['payload']) {
    await this.usersService.linkChat(payload);
  }
}
