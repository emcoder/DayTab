import { Module } from '@nestjs/common';
import { SlonikModule } from 'nestjs-slonik';
import { BotModule } from 'src/bot';
import { UsersController } from './users.contoller';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController],
  imports: [SlonikModule, BotModule],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
