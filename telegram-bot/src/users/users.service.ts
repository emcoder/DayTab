import { TgChatId, UserId } from '@daytab/common';
import { Injectable } from '@nestjs/common';
import { InjectPool } from 'nestjs-slonik';
import { DatabasePool, sql } from 'slonik';
import { BotService } from '../bot';

@Injectable()
export class UsersService {
  constructor(
    @InjectPool()
    private readonly databasePool: DatabasePool,
    private readonly botService: BotService,
  ) {}

  async findChatIds(userId: UserId): Promise<TgChatId[]> {
    const tgUsers = (await this.databasePool.many(
      sql.unsafe`SELECT chat_id AS chat_id FROM tg_users WHERE "user_id" = ${userId}`,
    )) as readonly { chat_id: TgChatId }[];
    const chatIds = tgUsers.map(({ chat_id }) => chat_id);
    return chatIds;
  }

  async linkChat(params: { userId: UserId; tgChatId: TgChatId }): Promise<void> {
    const { userId, tgChatId } = params;

    await this.databasePool.query(
      sql.unsafe`INSERT INTO tg_users(user_id, chat_id) VALUES (${userId}, ${tgChatId}) ON CONFLICT DO NOTHING`,
    );

    await this.botService.send({
      chatId: tgChatId,
      message: 'Your user successfully linked to Telegram!',
    });
  }
}
