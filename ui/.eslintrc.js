module.exports = {
  extends: [
    'airbnb',
    'airbnb-typescript',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    "plugin:import/recommended",
    "plugin:import/typescript",
    "plugin:prettier/recommended",
    'prettier',
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: [
    'unused-imports',
  ],
  settings: {
    'import/resolver': {
      typescript: true,
      node: true
    },
  },
  rules: {
    '@typescript-eslint/no-unused-vars': [
      'error',
      {
        args: 'all',
        argsIgnorePattern: '^_',
      },
    ],
    'class-methods-use-this': 'off',
    'import/prefer-default-export': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    'react/prefer-stateless-function': 0,
    '@typescript-eslint/unbound-method': 'off',
    'function-paren-newline': ['warn', 'consistent'],
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-unused-vars': 'off',
    'no-unused-vars': 'off',
    'unused-imports/no-unused-imports': 'error',
    'unused-imports/no-unused-vars': [
      'warn',
      {
        "vars": 'all',
        "varsIgnorePattern": '^_',
        "args": 'after-used',
        "argsIgnorePattern": '^_'
      }
    ],
    'import/no-named-as-default': 'off',
    'no-void': 'off',
    '@typescript-eslint/default-param-last': 'off',
    '@typescript-eslint/no-unsafe-argument': 'warn',
    '@typescript-eslint/no-floating-promises': 'off',
    'no-plusplus': 'off',
    'react/require-default-props': 'off',
    '@typescript-eslint/no-unused-expressions': ['error', { allowShortCircuit: true }],
    'react/jsx-props-no-spreading': 'off',
    'no-restricted-syntax': [
      'error',
      {
        selector: 'ForInStatement',
        message: 'for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.',
      },
      {
        selector: 'LabeledStatement',
        message: 'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.',
      },
      {
        selector: 'WithStatement',
        message: '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.',
      },
    ],
    'no-continue': 'off',
    '@typescript-eslint/restrict-template-expressions': 'off',
    'import/order': [
      'warn',
      {
        'newlines-between': 'always',
        'alphabetize': {
          'order': 'asc'
        },  
        "groups": [
          'unknown',
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
          'object',
          'type'
        ]
      },
    ],
  },
};
