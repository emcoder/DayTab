import { ChakraProvider } from '@chakra-ui/react';
import * as React from 'react';
import { useEffect } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import '@fontsource/fira-sans-condensed/400.css';
import '@fontsource/fira-sans-condensed/600.css';

import i18n from '@common/i18n';
import { logger } from '@common/logger';
import { refreshCurrentDay } from '@store/common';
import { refreshSession } from '@store/session';
import { refreshTasks } from '@store/tasks';
import { useAppDispatch } from '@store/types';

import { Main } from './Main';
import { Settings } from './Settings';
import { UserTelegramLink } from './Settings/user-telegram-link';
import { theme } from './style/common.style';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Main />,
  },
  {
    path: '/settings',
    element: <Settings />,
  },
  {
    path: '/user/link-telegram/:tgChatId',
    element: <UserTelegramLink />,
  },
]);

export function App() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    const refresh = async () => {
      const refreshSessionResult = await dispatch(refreshSession());
      if (refreshSession.rejected.match(refreshSessionResult)) {
        logger.error(`Failed to refresh session: ${refreshSessionResult.payload?.message}`);
        return;
      }

      const { language } = refreshSessionResult.payload;
      if (i18n.language !== language) {
        i18n.changeLanguage(language);
      }

      const refreshDayResult = await dispatch(refreshCurrentDay());

      if (refreshCurrentDay.rejected.match(refreshDayResult)) {
        logger.error(`Failed to refresh current day: ${refreshDayResult.payload?.message}`);
        return;
      }

      const { date } = refreshDayResult.payload;

      const refreshTasksResult = await dispatch(refreshTasks({ date }));
      if (refreshTasks.rejected.match(refreshTasksResult)) {
        logger.error(`Failed to refresh tasks list: ${refreshTasksResult.payload?.message}`);
      }
    };

    refresh();
  }, [dispatch]);

  return (
    <ChakraProvider theme={theme}>
      <RouterProvider router={router} />
    </ChakraProvider>
  );
}
