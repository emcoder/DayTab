import {
  Box,
  Button,
  StackDivider,
  VStack,
  Popover,
  PopoverContent,
  PopoverArrow,
  PopoverTrigger,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  useDisclosure,
  Portal,
} from '@chakra-ui/react';
import { Time } from '@daytab/common';
import { Field, FieldProps, Form, Formik } from 'formik';
import * as React from 'react';
import { shallowEqual, useSelector } from 'react-redux';

import { daysService } from '@core/days';
import { selectCurrentDay, updateDay } from '@store/common';
import { selectCurrentUser } from '@store/session';
import { refreshTasks } from '@store/tasks';
import { useAppDispatch } from '@store/types';

import { timeLineWidth } from '../TaskList/time-line/style/time-line.style';
import { BackgroundClickCatcher } from '../utils/popover-blur-click-preventer';

interface FormValues {
  start: string;
  end: string;
}

export function DayDurationSelector() {
  const user = useSelector(selectCurrentUser, shallowEqual);
  if (!user) {
    return null;
  }
  const { id: userId, timeZone } = user;

  const currentDay =
    useSelector(selectCurrentDay, shallowEqual) || daysService.createCurrentDay(userId, timeZone);
  const { date, startTime: start, endTime: end } = currentDay;

  const dispatch = useAppDispatch();

  const { isOpen, onToggle, onClose } = useDisclosure();

  const selectorForm = (
    <Formik
      initialValues={{ start, end }}
      enableReinitialize
      onSubmit={(values: FormValues, actions) => {
        const { start: newStart, end: newEnd } = values;

        dispatch(updateDay({ userId, date, startTime: newStart as Time, endTime: newEnd as Time }))
          .then(() => dispatch(refreshTasks({ date })))
          .then(() => {
            actions.setSubmitting(false);
            onClose();
          });
      }}
    >
      {(props) => (
        <Form>
          <VStack margin={3}>
            <Field name='start' type='time'>
              {({ field, form }: FieldProps<string, FormValues>) => (
                <FormControl isInvalid={Boolean(form.errors.start && form.touched.start)}>
                  <FormLabel>Start (HH:MM)</FormLabel>
                  {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                  <Input {...field} type='time' borderRadius='none' />
                  <FormErrorMessage>{form.errors.start}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <Field name='end' type='time'>
              {({ field, form }: FieldProps<string, FormValues>) => (
                <FormControl isInvalid={Boolean(form.errors.end && form.touched.end)}>
                  <FormLabel mt={2}>End (HH:MM)</FormLabel>
                  {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                  <Input {...field} type='time' borderRadius='none' />
                  <FormErrorMessage>{form.errors.end}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <FormControl>
              <Button
                mt={4}
                width='full'
                colorScheme='blue'
                isLoading={props.isSubmitting}
                type='submit'
                isDisabled={Boolean(!props.values.start || !props.values.end)}
                borderRadius='none'
              >
                Set duration
              </Button>
            </FormControl>
          </VStack>
        </Form>
      )}
    </Formik>
  );

  return (
    <Popover isOpen={isOpen} onClose={onClose}>
      <PopoverTrigger>
        <Button
          borderRadius='none'
          alignSelf='center'
          justifySelf='flex-start'
          height='full'
          width={timeLineWidth}
          minWidth={timeLineWidth}
          maxWidth={timeLineWidth}
          colorScheme='blue'
          onClick={onToggle}
        >
          <VStack direction='column' divider={<StackDivider borderColor='gray.200' />}>
            <Box>{start}</Box>
            <Box>{end}</Box>
          </VStack>
        </Button>
      </PopoverTrigger>
      <Portal>
        <PopoverContent borderRadius='none'>
          <PopoverArrow />
          {selectorForm}
        </PopoverContent>
        {isOpen && <BackgroundClickCatcher onClick={onClose} />}
      </Portal>
    </Popover>
  );
}
