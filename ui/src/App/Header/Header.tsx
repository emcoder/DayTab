import { Flex, IconButton } from '@chakra-ui/react';
import * as React from 'react';
import { MdOutlineArrowBack } from 'react-icons/md';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import { selectAuthorized } from '@store/session';

import { timeLineWidth } from '../TaskList/time-line';
import { RoutePath } from '../routes';
import { primaryColorScheme } from '../style';

import { DayDurationSelector } from './DayDurationSelector';
import { RefreshButton } from './RefreshButton';
import { SignInPopover } from './SignInPopover';
import { UserMenu } from './UserMenu';
import { DaySelector } from './day-selector/day-selector';
import { height, primaryBackgroundColor } from './style';

function BackToIndexButton() {
  const navigate = useNavigate();

  return (
    <IconButton
      height='full'
      borderRadius='none'
      width={timeLineWidth}
      minWidth={timeLineWidth}
      maxWidth={timeLineWidth}
      colorScheme={primaryColorScheme}
      icon={<MdOutlineArrowBack />}
      aria-label='Back'
      onClick={() => navigate(RoutePath.Index)}
    />
  );
}

function HeaderContainer(props: React.PropsWithChildren) {
  const { children } = props;

  return (
    <Flex
      width='100%'
      height={height}
      minHeight={height}
      maxHeight={height}
      bg={primaryBackgroundColor}
      direction='row'
      alignItems='center'
    >
      {children}
    </Flex>
  );
}

export function Header() {
  const authorized = useSelector(selectAuthorized);
  const location = useLocation();

  const pathname = location?.pathname;

  if (!authorized) {
    return (
      <HeaderContainer>
        <SignInPopover />
      </HeaderContainer>
    );
  }

  const leftButton =
    pathname && pathname === RoutePath.Index ? <DayDurationSelector /> : <BackToIndexButton />;

  return (
    <HeaderContainer>
      {leftButton}
      <DaySelector />
      <RefreshButton />
      <UserMenu />
    </HeaderContainer>
  );
}
