import { IconButton } from '@chakra-ui/react';
import * as React from 'react';
import { MdRefresh } from 'react-icons/md';
import { useSelector } from 'react-redux';

import { isRefreshPendingSelector, refreshDayWithTasks } from '@store/common';
import { useAppDispatch } from '@store/types';

import { height } from './style';

export function RefreshButton() {
  const dispatch = useAppDispatch();
  const isRefreshPending = useSelector(isRefreshPendingSelector);

  const handleClick = () => {
    dispatch(refreshDayWithTasks());
  };

  return (
    <IconButton
      // borderRadius='full'
      borderRadius='none'
      colorScheme='blue'
      height='full'
      width={height}
      variant='ghost'
      icon={<MdRefresh size='24' />}
      size='lg'
      aria-label='Refresh'
      onClick={handleClick}
      isLoading={isRefreshPending}
      color='white'
      _hover={{
        backgroundColor: 'blue.300',
      }}
    />
  );
}
