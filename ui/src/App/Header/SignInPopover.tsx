import { Popover, PopoverContent, PopoverArrow, PopoverTrigger, Button } from '@chakra-ui/react';
import * as React from 'react';
import { useTranslation } from 'react-i18next';

import { SignInForm } from '../SignInForm';
import { primaryColorScheme } from '../style';

export function SignInPopover() {
  const [t] = useTranslation();

  return (
    <Popover>
      <PopoverTrigger>
        <Button
          marginLeft='auto'
          marginRight={2}
          colorScheme={primaryColorScheme}
          borderRadius='none'
        >
          {t('sign_in')}
        </Button>
      </PopoverTrigger>
      <PopoverContent>
        <PopoverArrow />
        <SignInForm />
      </PopoverContent>
    </Popover>
  );
}
