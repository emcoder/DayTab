import {
  Avatar,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Box,
  Icon,
} from '@chakra-ui/react';
import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { MdFace } from 'react-icons/md';
import { useNavigate } from 'react-router-dom';

import { signOut } from '@store/session';
import { useAppDispatch } from '@store/types';

import { RoutePath } from '../routes';

export function UserAvatar() {
  return <Avatar icon={<Icon as={MdFace} />} size='lg' />;
}

export function UserMenu() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [t] = useTranslation();

  const handleSignOut = () => {
    dispatch(signOut()).then(() => navigate(RoutePath.Index));
  };

  return (
    <Box marginLeft='auto' paddingLeft={2} marginRight={2}>
      <Menu>
        <MenuButton as={IconButton} variant='link' icon={<UserAvatar />} />
        <MenuList borderRadius='none'>
          <MenuItem borderRadius='none' onClick={() => navigate(RoutePath.Settings)}>
            {t('settings')}
          </MenuItem>
          <MenuItem borderRadius='none' onClick={handleSignOut}>
            Sign Out
          </MenuItem>
        </MenuList>
      </Menu>
    </Box>
  );
}
