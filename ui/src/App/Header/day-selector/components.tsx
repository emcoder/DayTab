import { Button, Flex, forwardRef, IconButton, Text } from '@chakra-ui/react';
import { DayDate, TimeZone } from '@daytab/common';
import * as React from 'react';
import { MdOutlineArrowBackIos, MdOutlineArrowForwardIos } from 'react-icons/md';

import { formatDayDate } from './helpers';
import { getDateForDayDate, getCurrentDayDate } from '@common/helpers/date.helpers';

export function PrevDayButton({ onClick }: { onClick: () => void }) {
  return (
    <IconButton
      borderRadius='none'
      colorScheme='blue'
      height='full'
      icon={<MdOutlineArrowBackIos />}
      aria-label='Previous day'
      onClick={onClick}
    />
  );
}

export function NextDayButton({ onClick }: { onClick: () => void }) {
  return (
    <IconButton
      borderRadius='none'
      colorScheme='blue'
      height='full'
      icon={<MdOutlineArrowForwardIos />}
      aria-label='Next day'
      onClick={onClick}
    />
  );
}

export const DaySelectorButton = forwardRef(
  (
    props: {
      date: DayDate;
      timeZone: TimeZone;
      onClick: () => void;
    },
    ref,
  ) => {
    const { date, timeZone, onClick } = props;

    const { weekday: displayWeekday, date: displayDate } = formatDayDate(date, timeZone);

    const currentDate = getDateForDayDate(getCurrentDayDate(timeZone));
    const selectedDate = getDateForDayDate(date, timeZone);
    const isPast = selectedDate < currentDate;
    const displayDateParams = isPast ? { color: 'gray.300' } : {};

    return (
      <Button
        borderRadius='none'
        alignSelf='center'
        justifySelf='flex-start'
        height='full'
        colorScheme='blue'
        onClick={onClick}
        ref={ref}
      >
        <Flex direction='column'>
          <Text {...displayDateParams}>{displayWeekday}</Text>
          <Text {...displayDateParams}>{displayDate}</Text>
        </Flex>
      </Button>
    );
  },
);
