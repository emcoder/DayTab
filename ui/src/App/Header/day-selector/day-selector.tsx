import {
  Flex,
  IconButton,
  Input,
  Popover,
  PopoverContent,
  PopoverArrow,
  PopoverTrigger,
  useDisclosure,
  FormControl,
  FormErrorMessage,
  PopoverBody,
  Portal,
} from '@chakra-ui/react';
// import { DayDateType } from '@daytab/common';
import { DayDate } from '@daytab/common';
import { Field, FieldProps, Form, Formik, FormikErrors, FormikHelpers, FormikProps } from 'formik';
import { isLeft, right } from 'fp-ts/Either';
import * as React from 'react';
import { MdCheck, MdOutlineAccessTime } from 'react-icons/md';
import { shallowEqual, useSelector } from 'react-redux';

import {
  getCurrentDayDate,
  isValidDateString,
  getNextDayDate,
  getPrevDayDate,
} from '@common/helpers/date.helpers';
import { logger } from '@common/logger';
import { daysService } from '@core/days';
import { selectCurrentDay } from '@store/common';
import { selectCurrentUser } from '@store/session';
import { useAppDispatch } from '@store/types';

import { BackgroundClickCatcher } from '../../utils/popover-blur-click-preventer';

import { DaySelectorButton, NextDayButton, PrevDayButton } from './components';
import { processSelectDay } from './helpers';

interface FormValues {
  date: string;
}

export function DaySelector() {
  const dispatch = useAppDispatch();
  const user = useSelector(selectCurrentUser, shallowEqual);
  if (!user) {
    return null;
  }
  const { id: userId, timeZone } = user;

  const currentDay =
    useSelector(selectCurrentDay) || daysService.createCurrentDay(userId, timeZone);
  const { date } = currentDay;

  const { isOpen, onToggle, onClose } = useDisclosure();

  const handleSelectDay = ({ date: newDate }: FormValues, actions: FormikHelpers<FormValues>) => {
    // const dayDateValidation = DayDateType.decode(newDate);
    const dayDateValidation = right(newDate as DayDate);
    if (isLeft(dayDateValidation)) {
      actions.setErrors({
        date: `Incorrect date "${newDate}"`,
      });
      return;
    }
    const dayDate = dayDateValidation.right;

    processSelectDay({ dayDate, dispatch })
      .then(() => {
        actions.setSubmitting(false);
        onClose();
      })
      .catch((err: Error) => {
        actions.setSubmitting(false);
        actions.setErrors({
          date: `Failed to select day ${dayDate}: ${err.message}`,
        });
      });
  };

  const handleSetPrevDay = () => {
    const prevDayDate = getPrevDayDate(date, timeZone);
    processSelectDay({ dayDate: prevDayDate, dispatch }).catch((err: Error) => {
      logger.error(`Failed to select day ${prevDayDate}: ${err.message}`);
    });
  };

  const handleSetNextDay = () => {
    const nextDayDate = getNextDayDate(date, timeZone);
    processSelectDay({ dayDate: nextDayDate, dispatch }).catch((err: Error) => {
      logger.error(`Failed to select day ${nextDayDate}: ${err.message}`);
    });
  };

  return (
    <Flex direction='row' marginLeft='auto' alignItems='center' justifyItems='center' height='full'>
      <PrevDayButton onClick={handleSetPrevDay} />
      <Popover isOpen={isOpen} onClose={onClose}>
        <PopoverTrigger>
          <DaySelectorButton onClick={onToggle} date={date} timeZone={timeZone} />
        </PopoverTrigger>
        <Portal>
          <PopoverContent borderRadius='none'>
            <PopoverArrow />
            <PopoverBody>
              <Formik
                initialValues={{ date: date as string }}
                enableReinitialize
                onSubmit={handleSelectDay}
                validateOnChange
                validate={({ date: newDate }): FormikErrors<{ date: string }> => {
                  if (!newDate) {
                    return {
                      date: 'Empty date',
                    };
                  }

                  if (!isValidDateString(newDate)) {
                    return {
                      date: `Invalid date "${newDate}"`,
                    };
                  }

                  return {};
                }}
              >
                {({
                  isSubmitting,
                  values,
                  errors,
                  setFieldValue,
                  submitForm,
                }: FormikProps<FormValues>) => (
                  <Form style={{ display: 'flex', flexDirection: 'row' }}>
                    <Field name='date'>
                      {({ field, form }: FieldProps<string, FormValues>) => (
                        <FormControl isInvalid={Boolean(form.errors.date && form.touched.date)}>
                          <Input
                            {...field}
                            borderRadius='none'
                            colorScheme='blue'
                            type='date'
                            fontWeight='bold'
                            textAlign='center'
                          />
                          <FormErrorMessage>{form.errors.date}</FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                    <IconButton
                      colorScheme='blue'
                      variant='ghost'
                      borderRadius='none'
                      type='submit'
                      isLoading={isSubmitting}
                      isDisabled={Boolean(errors.date || !values.date)}
                      aria-label='Select'
                      icon={<MdCheck />}
                    />
                    <IconButton
                      colorScheme='blue'
                      variant='ghost'
                      borderRadius='none'
                      disabled={isSubmitting}
                      isDisabled={Boolean(errors.date || !values.date)}
                      aria-label='Set to now'
                      icon={<MdOutlineAccessTime />}
                      onClick={() => {
                        const current = getCurrentDayDate();
                        setFieldValue('date', current);
                        submitForm();
                      }}
                    />
                  </Form>
                )}
              </Formik>
            </PopoverBody>
          </PopoverContent>
          {isOpen && <BackgroundClickCatcher onClick={onClose} />}
        </Portal>
      </Popover>
      <NextDayButton onClick={handleSetNextDay} />
    </Flex>
  );
}
