import { DayDate, TimeZone } from '@daytab/common';
import { formatInTimeZone } from 'date-fns-tz';

import { getDateForDayDate } from '@common/helpers/date.helpers';
import { getBrowserTimeZone } from '@common/helpers/time-zone.helpers';
import { selectDay } from '@store/common';
import { refreshTasks } from '@store/tasks';
import { AppDispatch } from '@store/types';

export function formatDayDate(
  dayDate: DayDate,
  timeZone?: TimeZone,
): {
  date: string;
  weekday: string;
} {
  const tz = timeZone || getBrowserTimeZone();
  const currDate = getDateForDayDate(dayDate, tz);

  const weekday = formatInTimeZone(currDate, tz, 'EEEE');
  const date = formatInTimeZone(currDate, tz, 'd MMMM');

  return {
    date,
    weekday,
  };
}

export async function processSelectDay(params: { dayDate: DayDate; dispatch: AppDispatch }) {
  const { dayDate, dispatch } = params;

  const dayResult = await dispatch(selectDay(dayDate));
  if (selectDay.rejected.match(dayResult)) {
    const { payload: { message } = {} } = dayResult;
    throw new Error(`Failed to select day "${dayDate}": ${message}`);
  }

  const {
    payload: { date: newDayDate },
  } = dayResult;
  const refreshTasksResult = await dispatch(refreshTasks({ date: newDayDate }));
  if (refreshTasks.rejected.match(refreshTasksResult)) {
    const { payload: { message } = {} } = refreshTasksResult;
    throw new Error(`Failed to refresh tasks for day ${newDayDate}: ${message}`);
  }
}
