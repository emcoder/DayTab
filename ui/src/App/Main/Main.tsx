import { Center, Flex } from '@chakra-ui/react';
import * as React from 'react';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';

import { selectAuthorized } from '@store/session';

import { Header } from '../Header/Header';
import { SignInForm } from '../SignInForm';
import { TaskList } from '../TaskList';
import { getBackgroundStyle } from '../style';

export function Main() {
  const authorized = useSelector(selectAuthorized);

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const redirect = searchParams.get('redirect');

  useEffect(() => {
    if (redirect && authorized) {
      navigate(decodeURIComponent(redirect));
    }
  }, [authorized, redirect]);

  return authorized ? (
    <>
      <Header />
      <Flex flexGrow={1} margin='0' width='full' min-height='full' maxWidth='full' padding='0'>
        <TaskList />
      </Flex>
    </>
  ) : (
    <Center height='full' flex={1} sx={getBackgroundStyle()}>
      <SignInForm />
    </Center>
  );
}
