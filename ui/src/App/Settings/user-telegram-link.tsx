import { Center, Spinner } from '@chakra-ui/react';
import { TgChatIdType } from '@daytab/common';
import { isLeft } from 'fp-ts/lib/Either';
import * as React from 'react';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

import { logger } from '@common/logger';
import { selectAuthorized } from '@store/session';
import { userLinkTelegram } from '@store/settings';
import { useAppDispatch } from '@store/types';

export function UserTelegramLink() {
  const { tgChatId } = useParams();
  const authorized = useSelector(selectAuthorized);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const location = useLocation();

  useEffect(() => {
    const tgChatIdResult = TgChatIdType.decode(Number(tgChatId));

    if (!authorized) {
      logger.error('Unauthorized in use tg link');
      const redirect = `${location.pathname}${location.search}`;
      navigate(`/?redirect=${encodeURIComponent(redirect)}`);
      return;
    }

    if (isLeft(tgChatIdResult)) {
      logger.error(`Incorrect telegram chat ID provided: ${tgChatId}`);
      navigate('/');
    } else {
      dispatch(userLinkTelegram({ tgChatId: tgChatIdResult.right })).then((result) => {
        if (userLinkTelegram.rejected.match(result)) {
          logger.error(`Failed to link user to Telegram: ${result.payload?.message}`);
        } else {
          logger.info(`User linked to Telegram`);
        }
        navigate('/');
      });
    }
  }, []);

  return (
    <Center width='full' height='full'>
      <Spinner />
    </Center>
  );
}
