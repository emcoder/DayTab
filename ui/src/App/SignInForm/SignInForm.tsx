import {
  Button,
  Card,
  CardBody,
  Center,
  FormControl,
  FormErrorMessage,
  Input,
  InputGroup,
  InputLeftElement,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useMediaQuery,
  VStack,
} from '@chakra-ui/react';
import { Field, FieldProps, Form, Formik } from 'formik';
import * as React from 'react';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { MdAccountBox, MdPassword } from 'react-icons/md';

import { logger } from '@common/logger';
import { refreshCurrentDay } from '@store/common';
import { signIn } from '@store/session';
import { refreshTasks } from '@store/tasks';
import { useAppDispatch } from '@store/types';

import { LanguageSelector } from '../common/language-selector';
import { smallScreenMediaQuery } from '../style';

interface FormValues {
  username: string;
  password: string;
}

export function SignInForm() {
  const dispatch = useAppDispatch();
  const [state, setState] = useState<{ error: string | null }>({
    error: null,
  });

  const [isSmallScreen] = useMediaQuery(smallScreenMediaQuery);
  const [t] = useTranslation();

  const signInForm = (
    <Formik
      initialValues={{ username: '', password: '' } as FormValues}
      onSubmit={async (values, actions) => {
        const { username, password } = values;

        const result = await dispatch(signIn({ username, password }));
        actions.setSubmitting(false);

        if (signIn.rejected.match(result)) {
          setState({
            error: result.payload?.message || 'Unknown error',
          });
          return;
        }

        setState({
          error: null,
        });
        const refreshDayResult = await dispatch(refreshCurrentDay());

        if (refreshCurrentDay.rejected.match(refreshDayResult)) {
          logger.error(`Failed to refresh current day: ${refreshDayResult.payload?.message}`);
          return;
        }

        const { date } = refreshDayResult.payload;

        const refreshTasksResult = await dispatch(refreshTasks({ date }));
        if (refreshTasks.rejected.match(refreshTasksResult)) {
          logger.error(`Failed to refresh tasks list: ${refreshTasksResult.payload?.message}`);
        }
      }}
    >
      {(props) => (
        <Form>
          <VStack spacing={5}>
            <Field name='username'>
              {({ field, form }: FieldProps<string, FormValues>) => (
                <FormControl isInvalid={Boolean(form.errors.username && form.touched.username)}>
                  {/* <FormLabel>Username</FormLabel> */}
                  {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                  <InputGroup>
                    <InputLeftElement pointerEvents='none' color='blue.600'>
                      <MdAccountBox />
                    </InputLeftElement>
                    <Input {...field} borderRadius='none' placeholder={t('username')} />
                  </InputGroup>
                  <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <Field name='password' type='password'>
              {({ field, form }: FieldProps<string, FormValues>) => (
                <FormControl isInvalid={Boolean(form.errors.password && form.touched.password)}>
                  <InputGroup>
                    <InputLeftElement pointerEvents='none' color='blue.600'>
                      <MdPassword />
                    </InputLeftElement>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <Input
                      {...field}
                      borderRadius='none'
                      type='password'
                      placeholder={t('password')}
                    />
                  </InputGroup>
                  <FormErrorMessage>{form.errors.password}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <FormControl isInvalid={Boolean(state.error)}>
              <Button
                width='full'
                colorScheme='blue'
                isLoading={props.isSubmitting}
                type='submit'
                isDisabled={Boolean(!props.values.username || !props.values.password)}
                borderRadius='none'
              >
                {t('sign_in')}
              </Button>
              <FormErrorMessage width='100%' overflowWrap='break-word' overflow='hidden'>
                {state.error}
              </FormErrorMessage>
            </FormControl>
          </VStack>
        </Form>
      )}
    </Formik>
  );

  const cardProps = isSmallScreen
    ? { shadow: 'none', width: 'full' }
    : { minHeight: 'xs', width: 'md' };
  return (
    <Card borderRadius='none' backgroundColor='white' minHeight='xs' {...cardProps}>
      <CardBody display='flex' flexDirection='column'>
        <Tabs size='lg' variant='line' isFitted height='full' display='flex' flexDirection='column'>
          <TabList>
            <Tab>{t('sign_in')}</Tab>
            <Tab>{t('sign_up')}</Tab>
          </TabList>

          <TabPanels minHeight='14rem'>
            <TabPanel padding='none' paddingTop={5} paddingBottom={5}>
              {signInForm}
            </TabPanel>
            <TabPanel padding='none' paddingTop={5} paddingBottom={5}>
              <Center>Registration temporarily unavailable</Center>
            </TabPanel>
          </TabPanels>
        </Tabs>
        <LanguageSelector justifySelf='end' alignSelf='end' />
      </CardBody>
    </Card>
  );
}
