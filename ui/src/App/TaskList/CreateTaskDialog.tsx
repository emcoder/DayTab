import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  useMediaQuery,
} from '@chakra-ui/react';
import * as React from 'react';

import { smallScreenMediaQuery } from '../style';

import { CreateTaskForm } from './CreateTaskForm';

export interface CreateTaskDialogProps {
  isOpen: boolean;
  onClose: () => void;
}

export function CreateTaskDialog(props: CreateTaskDialogProps) {
  const { isOpen, onClose } = props;

  const [isSmallScreen] = useMediaQuery(smallScreenMediaQuery);

  const modalStyle = isSmallScreen ? { size: 'full' } : {};

  return (
    <Modal isOpen={isOpen} onClose={onClose} {...modalStyle}>
      <ModalOverlay />
      <ModalContent borderRadius='none'>
        <ModalHeader>Add task</ModalHeader>
        <ModalCloseButton borderRadius='none' />
        <ModalBody>
          <CreateTaskForm onAdd={onClose} onDelete={onClose} />
        </ModalBody>

        {/* <ModalFooter>
        <Button colorScheme='blue' mr={3} onClick={onClose}>
          Close
        </Button>
        <Button variant='ghost'>Secondary Action</Button>
      </ModalFooter> */}
      </ModalContent>
    </Modal>
  );
}
