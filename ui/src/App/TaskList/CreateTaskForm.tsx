import {
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Spacer,
  VStack,
} from '@chakra-ui/react';
import { Time, TimeType } from '@daytab/common';
import { Field, FieldProps, Form, Formik, FormikProps } from 'formik';
import { isLeft } from 'fp-ts/Either';
import { useState } from 'react';
import * as React from 'react';
import { shallowEqual, useSelector } from 'react-redux';

import { addMinutesToTime, getCurrentTime, getTime } from '@common/helpers/date.helpers';
import { DEFAULT_DAY_END_TIME } from '@core/days';
import { getTaskStartEnd, Task } from '@core/tasks';
import { Minutes } from '@core/types';
import { selectCurrentDay } from '@store/common';
import { selectCurrentUser, selectDefaultTaskDuration } from '@store/session';
import { createTask, deleteTask, updateTask } from '@store/tasks';
import { useAppDispatch } from '@store/types';

export interface FormValues {
  name: string;
  start: string;
  end: string;
}

export interface CreateTaskFormProps {
  start?: Time;
  duration?: Minutes;
  onAdd?: () => void;
  isUpdate?: boolean;
}

export interface UpdateTaskFormProps {
  id: Task['id'];
  name: string;
  start: Date;
  end: Date;
  onSave?: () => void;
  onDelete?: () => void;
}

function DeleteButton({ onClick }: { onClick: () => void }) {
  return (
    <Button minWidth={32} colorScheme='red' onClick={onClick} borderRadius='none'>
      Delete
    </Button>
  );
}

function FormComponent(
  props: FormikProps<FormValues> & {
    error: string | null;
    isUpdate: boolean;
    onDelete: () => void;
  },
) {
  const { error, values, isSubmitting, isUpdate, onDelete } = props;

  return (
    <Form style={{ minHeight: '100%' }}>
      <VStack height='full'>
        <Field name='name' required>
          {({ field, form }: FieldProps<string, FormValues>) => (
            <FormControl isInvalid={Boolean(form.errors.name && form.touched.name)}>
              <FormLabel>Title</FormLabel>
              {/* eslint-disable-next-line react/jsx-props-no-spreading */}
              <Input {...field} borderRadius='none' placeholder='Task name' />
              <FormErrorMessage>{form.errors.name}</FormErrorMessage>
            </FormControl>
          )}
        </Field>
        <Field name='start' type='time' required>
          {({ field, form }: FieldProps<string, FormValues>) => (
            <FormControl isInvalid={Boolean(form.errors.start && form.touched.start)}>
              <FormLabel>Start (HH:MM)</FormLabel>
              {/* eslint-disable-next-line react/jsx-props-no-spreading */}
              <Input {...field} type='time' borderRadius='none' placeholder='Start' />
              <FormErrorMessage>{form.errors.start}</FormErrorMessage>
            </FormControl>
          )}
        </Field>
        <Field name='end' type='time' required>
          {({ field, form }: FieldProps<string, FormValues>) => (
            <FormControl isInvalid={Boolean(form.errors.end && form.touched.end)}>
              <FormLabel mt={2}>End (HH:MM)</FormLabel>
              {/* eslint-disable-next-line react/jsx-props-no-spreading */}
              <Input {...field} borderRadius='none' type='time' placeholder='End' />
              <FormErrorMessage>{form.errors.end}</FormErrorMessage>
            </FormControl>
          )}
        </Field>
      </VStack>
      <Flex direction='row' margin='none' marginTop={8} marginBottom={4} width='full' gap={4}>
        {isUpdate && <DeleteButton onClick={onDelete} />}
        <Spacer />
        <FormControl isInvalid={Boolean(error)} width='auto'>
          <Button
            minWidth={32}
            colorScheme='blue'
            isLoading={isSubmitting}
            type='submit'
            isDisabled={Boolean(!values.name || !values.start || !values.end)}
            borderRadius='none'
          >
            {isUpdate ? 'Save' : 'Add'}
          </Button>
          <FormErrorMessage width='100%' overflowWrap='break-word' overflow='hidden'>
            {error}
          </FormErrorMessage>
        </FormControl>
      </Flex>
    </Form>
  );
}

export function CreateTaskForm(props: CreateTaskFormProps | UpdateTaskFormProps) {
  const dispatch = useAppDispatch();
  const [state, setState] = useState<{ error: string | null }>({
    error: null,
  });

  const day = useSelector(selectCurrentDay, shallowEqual);
  if (!day) {
    throw new Error('Current day is required');
  }
  const { date } = day;

  const user = useSelector(selectCurrentUser, shallowEqual);
  if (!user) {
    throw new Error(`Current user is not provided`);
  }
  const tz = user.timeZone;

  let initialStart: Time;
  let initialEnd: Time;
  let initialName: string;
  let handleSubmit: () => void;
  let isUpdate = false;
  let taskId: number | null = null;
  let handleDelete: () => void = () => null;

  if ('id' in props) {
    const { id, name, start, end, onSave, onDelete } = props;
    taskId = id;
    initialStart = getTime(start, tz);
    initialEnd = getTime(end, tz);
    initialName = name;
    if (onSave) {
      handleSubmit = onSave;
    }
    isUpdate = true;

    handleDelete = () => {
      dispatch(deleteTask({ id }));
      onDelete && onDelete();
    };
  } else {
    const { onAdd, duration } = props;

    const defaultTaskDuration = useSelector(selectDefaultTaskDuration);
    const taskDuration = duration || defaultTaskDuration;

    initialStart = getCurrentTime(tz);
    initialEnd = addMinutesToTime(initialStart, taskDuration);
    initialName = '';
    if (onAdd) {
      handleSubmit = onAdd;
    }
  }

  const { error } = state;

  const handleUpdate = async (params: { id: Task['id']; name: string; start: Date; end: Date }) => {
    const result = await dispatch(updateTask(params));

    if (updateTask.fulfilled.match(result)) {
      setState({
        error: null,
      });
    } else if (updateTask.rejected.match(result)) {
      setState({
        error: result.payload?.message || result.error.message || 'Unknown error',
      });
    }
  };

  const handleCreate = async (params: { name: string; start: Date; end: Date }) => {
    const result = await dispatch(createTask(params));

    if (createTask.fulfilled.match(result)) {
      setState({
        error: null,
      });
    } else if (createTask.rejected.match(result)) {
      setState({
        error: result.payload?.message || result.error.message || 'Unknown error',
      });
    }
  };

  return (
    <Formik
      initialValues={{ name: initialName, start: initialStart, end: initialEnd } as FormValues}
      onSubmit={async (values, actions) => {
        const { name, start, end } = values;

        const startTimeResult = TimeType.decode(start);
        if (isLeft(startTimeResult)) {
          actions.setErrors({ start: `Invalid start time` });
          return;
        }
        const startTime = startTimeResult.right;

        const endTimeResult = TimeType.decode(end);
        if (isLeft(endTimeResult)) {
          actions.setErrors({ end: 'Invalid end time' });
          return;
        }
        const endTime = endTimeResult.right;

        const { start: startDate, end: endDate } = getTaskStartEnd({
          dayDate: date,
          startTime,
          endTime,
          timeZone: tz,
          dayEndTime: DEFAULT_DAY_END_TIME,
        });

        if (taskId) {
          await handleUpdate({ id: taskId, name, start: startDate, end: endDate });
        } else {
          await handleCreate({ name, start: startDate, end: endDate });
        }

        actions.setSubmitting(false);

        handleSubmit && handleSubmit();
      }}
    >
      {(formProps) => (
        <FormComponent {...formProps} isUpdate={isUpdate} error={error} onDelete={handleDelete} />
      )}
    </Formik>
  );
}
