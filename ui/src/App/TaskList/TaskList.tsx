import { Flex, Spinner, useDisclosure } from '@chakra-ui/react';
import { parseISO } from 'date-fns';
import * as React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { useSwipeable } from 'react-swipeable';

import { mapAndGroupBy } from '@common/helpers/map.helpers';
import { refreshDayWithTasks, selectCurrentDay } from '@store/common';
import { selectCurrentUser } from '@store/session';
import { selectCurrentTasks } from '@store/tasks';
import { useAppDispatch } from '@store/types';

import { getBackgroundStyle } from '../style';

import { CreateTaskDialog } from './CreateTaskDialog';
import { TaskItem } from './task-item';
import { TimeLine } from './time-line';

export function TaskList() {
  const currentTasks = useSelector(selectCurrentTasks, shallowEqual);
  const dispatch = useAppDispatch();

  const { isOpen, onOpen, onClose } = useDisclosure({ id: 'task-create' });

  const currentDay = useSelector(selectCurrentDay, shallowEqual);

  const swipeHandlers = useSwipeable({
    onSwipedDown: () => {
      dispatch(refreshDayWithTasks());
    },
    preventScrollOnSwipe: true,
    delta: {
      up: 200,
      down: 200,
    },
  });

  const user = useSelector(selectCurrentUser, shallowEqual);

  if (!currentDay) {
    return <Spinner />;
  }

  if (!user) {
    return <Spinner />;
  }

  const handleFreeSpaceClick = () => {
    onOpen();
  };

  const tasksByLevels = mapAndGroupBy(currentTasks, 'level');
  const taskLevels = Array.from(tasksByLevels.values()).map((levelTasks, index) => {
    const taskItems = levelTasks.map(({ id, name, start, end, color, size, offset }) => {
      return (
        <TaskItem
          id={id}
          offset={offset}
          size={size}
          key={`task-${id}`}
          name={name}
          start={parseISO(start)}
          end={parseISO(end)}
          color={color}
        />
      );
    });

    return (
      // eslint-disable-next-line react/no-array-index-key
      <Flex direction='column' key={`tasks-level-${index}`} flex={1} minWidth='2xs'>
        {taskItems}
      </Flex>
    );
  });

  return (
    <Flex direction='row' minHeight='full' maxWidth='full' grow={1} {...swipeHandlers}>
      <TimeLine />
      <CreateTaskDialog isOpen={isOpen} onClose={onClose} />
      <Flex
        direction='row'
        onClick={handleFreeSpaceClick}
        flex={1}
        flexBasis={0}
        sx={getBackgroundStyle()}
        overflowX='scroll'
        maxWidth='100%'
      >
        {taskLevels}
      </Flex>
    </Flex>
  );
}
