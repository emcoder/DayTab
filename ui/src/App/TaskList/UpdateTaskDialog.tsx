import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  useMediaQuery,
} from '@chakra-ui/react';
import * as React from 'react';

import { Task } from '@core/tasks';

import { smallScreenMediaQuery } from '../style';

import { CreateTaskForm } from './CreateTaskForm';

export interface UpdateTaskDialogProps {
  isOpen: boolean;
  onClose: () => void;
  id: Task['id'];
  name: string;
  start: Date;
  end: Date;
}

export function UpdateTaskDialog(props: UpdateTaskDialogProps) {
  const { isOpen, onClose, id, name, start, end } = props;

  const [isSmallScreen] = useMediaQuery(smallScreenMediaQuery);

  const modalStyle = isSmallScreen ? { size: 'full' } : {};

  return (
    <Modal isOpen={isOpen} onClose={onClose} {...modalStyle}>
      <ModalOverlay />
      <ModalContent borderRadius='none'>
        <ModalHeader>Update task</ModalHeader>
        <ModalCloseButton borderRadius='none' />
        <ModalBody>
          <CreateTaskForm
            id={id}
            name={name}
            start={start}
            end={end}
            onSave={onClose}
            onDelete={onClose}
          />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
