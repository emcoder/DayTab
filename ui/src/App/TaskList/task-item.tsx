import { Box, Card, Center, Flex, Grid, GridItem, Text, useDisclosure } from '@chakra-ui/react';
import { Time } from '@daytab/common';
import * as React from 'react';
import { useSelector } from 'react-redux';

import { formatDateDiff, getTime } from '@common/helpers/date.helpers';
import { formatSize } from '@common/helpers/size.helpers';
import { parseToType } from '@common/helpers/type.helpers';
import { Color, Size, SizeType } from '@core/types';
import { selectCurrentUser } from '@store/session';

import { getTaskTextColor, minimumTaskSize } from '../style';

import { UpdateTaskDialog } from './UpdateTaskDialog';

import type { Task } from '@core/tasks';

export interface TaskItemProps {
  id: Task['id'];
  name: string;
  start: Date;
  end: Date;
  color: Color;
  offset: Size;
  size: Size;
}

function TaskTime(props: { time: Time, isPast: boolean }) {
  const { time, isPast } = props;
  return (
    <Center fontSize='xs' boxSize='full' textColor={getTaskTextColor(isPast)} fontWeight='semibold' padding='1'>
      {time}
    </Center>
  );
}

function TaskDuration(props: { start: Date; end: Date; isPast: boolean }) {
  const { start, end, isPast } = props;

  const duration = formatDateDiff(start, end);

  return (
    <Center boxSize='full' textColor={getTaskTextColor(isPast)} fontWeight='semibold' fontSize='sm' padding={1}>
      {duration}
    </Center>
  );
}

export function TaskItem(props: TaskItemProps) {
  const { id, name, start, end, color, offset, size } = props;

  const { isOpen, onOpen, onClose } = useDisclosure({ id: 'task-update' });

  const user = useSelector(selectCurrentUser);
  if (!user) {
    throw new Error('User data missing');
  }
  const { timeZone } = user;

  const startTime = getTime(start, timeZone);
  const endTime = getTime(end, timeZone);
  const isPast = end < new Date();

  const height = formatSize(size);
  const cardHeight = formatSize(parseToType(SizeType, minimumTaskSize - 1));

  const handleClick = (event: React.SyntheticEvent) => {
    event.stopPropagation();
    onOpen();
  };

  return (
    <Box
      borderRadius='none'
      marginTop={formatSize(offset)}
      backgroundColor={color}
      minHeight={height}
      maxHeight={height}
      // borderTop='1px solid'
      // borderTopColor={colorCode}
      onClick={handleClick}
    >
      <Flex direction='row' height='full' alignItems='flex-start'>
        <Box backgroundColor={color} minWidth={2} width={2} height='full' minHeight='full' />
        <Card
          display='flex'
          flexDirection='row'
          borderRadius='none'
          margin={0}
          marginBottom='1px'
          backgroundColor='white'
          // borderBottom='1px solid'
          boxShadow='base'
          // borderTop='1px solid'
          // borderTopColor={colorCode}
          // borderBottomColor='gray.200'
          // borderBottom='1px solid'
          // borderBottomColor={colorCode}
          width='full'
          height='full'
          maxHeight={cardHeight}
          boxSizing='border-box'
        >
          <Grid
            width='20'
            templateRows='repeat(2, 1fr)'
            templateColumns='repeat(2, 1fr)'
            borderRight='1px solid'
            borderColor='gray.300'
          >
            <GridItem display='flex' borderRight='1px solid' borderColor='gray.300'>
              <TaskTime time={startTime} isPast={isPast} />
            </GridItem>
            <GridItem display='flex' alignContent='center' justifyContent='center'>
              <TaskTime time={endTime} isPast={isPast} />
            </GridItem>
            <GridItem
              colSpan={2}
              display='flex'
              alignContent='center'
              justifyContent='center'
              borderTop='1px solid'
              borderColor='gray.300'
            >
              <TaskDuration start={start} end={end} isPast={isPast} />
            </GridItem>
          </Grid>
          <Text padding={2} color={getTaskTextColor(isPast)}>{name}</Text>
        </Card>
      </Flex>
      <UpdateTaskDialog
        isOpen={isOpen}
        onClose={onClose}
        id={id}
        name={name}
        start={start}
        end={end}
      />
    </Box>
  );
}
