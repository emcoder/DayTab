import { Size } from '@core/types';

export function getTimeLineOverflowBlockBackground() {
  const stripeColor = `#5093bf`;
  return {
    backgroundColor: '#63b3ed',
    backgroundImage: [
      `repeating-linear-gradient(
        45deg,
        ${stripeColor} 0px,
        ${stripeColor} 1px,
        transparent 1px,
        transparent 5px
      )`,
    ].join(','),
    backgroundSize: [`100% 100%`].join(','),
    backgroundRepeat: 'repeat',
    backgroundPosition: ['top left'].join(','),
    backgroundClip: 'content-box',
  };
}

export function getTimeLineBlockBackground(size: Size, striped?: boolean) {
  const smallTickColor = '#9bcaed';
  const boldTickColor = '#93cded';

  const smallInterval = Math.round(size / 4);
  const smallPx = `${smallInterval}px`;

  const middleInterval = Math.round(size / 2);
  const boldPx = `${middleInterval}px`;

  if (striped) {
    const stripeColor = `#5093bf`;
    return {
      backgroundImage: [
        `linear-gradient(${boldTickColor} 0 1px, transparent 1px 100%)`,
        `linear-gradient(${smallTickColor} 0 0.5px, transparent 0.5px 100%)`,
        `repeating-linear-gradient(
          -45deg,
          ${stripeColor} 0px,
          ${stripeColor} 1px,
          transparent 1px,
          transparent 5px
        )`,
      ].join(','),
      backgroundSize: [`15% ${boldPx}`, `10% ${smallPx}`, `100% 100%`].join(','),
      backgroundRepeat: 'repeat-y, repeat-y, repeat',
      backgroundPosition: ['right top -0.9px', 'right top -0.9px', 'top left'].join(','),
      backgroundClip: 'content-box, content-box, content-box',
    };
  }

  return {
    backgroundImage: [
      `linear-gradient(${boldTickColor} 0 1px, transparent 1px 100%)`,
      `linear-gradient(${smallTickColor} 0 0.5px, transparent 0.5px 100%)`,
    ].join(','),
    backgroundSize: [`15% ${boldPx}`, `10% ${smallPx}`].join(','),
    backgroundRepeat: 'repeat-y',
    backgroundPosition: ['right top -0.9px', 'right top -0.9px'].join(','),
    backgroundClip: 'content-box, content-box',
  };
}
