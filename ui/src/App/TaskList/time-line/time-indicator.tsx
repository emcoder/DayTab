import { Box } from '@chakra-ui/react';
import * as React from 'react';

import { formatSize } from '@common/helpers/size.helpers';
import { Size } from '@core/types';

import {
  timeIndicatorBackgroundColor,
  timeIndicatorMarkColor,
  timeLineWidth,
} from './style/time-line.style';

export function CurrentTimeIndicator(props: { position: Size; isPast: boolean }) {
  const { position, isPast } = props;
  return (
    <Box
      width={timeLineWidth}
      height={formatSize(position)}
      pointerEvents='none'
      backgroundColor={timeIndicatorBackgroundColor}
      position='absolute'
      {...(!isPast ? { borderBottom: `2px solid ${timeIndicatorMarkColor}` } : {})}
    />
  );
}
