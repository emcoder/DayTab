import { Flex, Spacer, Text } from '@chakra-ui/react';
import * as React from 'react';

import { formatSize } from '@common/helpers/size.helpers';
import { TimeLineBlock } from '@core/time-line';

import { TIME_LINE_BLOCK_SIZE_MINIMUM } from '../../style';

import { getTimeLineBlockBackground } from './style/get-time-line-block-background';

export type TimeLineItemProps = Pick<
  TimeLineBlock,
  'size' | 'startTime' | 'endTime' | 'isAfter' | 'isBefore'
> & {
  isLast: boolean;
};

export function TimeLineItem(props: TimeLineItemProps) {
  const { size, startTime, endTime, isAfter, isBefore, isLast } = props;

  const height = formatSize(size);

  const endTimeText = (isAfter || isLast) && endTime ? <Text>{endTime}</Text> : null;

  return (
    <Flex
      direction='column'
      height={height}
      minHeight={height}
      maxHeight={height}
      borderBottom='1px solid'
      borderBottomColor='#5ba5d9'
      key={startTime}
      fontWeight='bold'
      textColor='whitesmoke'
      alignItems='center'
      boxSizing='border-box'
      bgColor='blue.300'
      sx={getTimeLineBlockBackground(size || TIME_LINE_BLOCK_SIZE_MINIMUM, isBefore || isAfter)}
    >
      <Text marginTop={2}>{startTime}</Text>
      <Spacer />
      {endTimeText}
    </Flex>
  );
}
