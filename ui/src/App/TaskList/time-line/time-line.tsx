import { Flex } from '@chakra-ui/react';
import { parseISO } from 'date-fns';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { shallowEqual, useSelector } from 'react-redux';

import { diffInMinutes } from '@common/helpers/time.helpers';
import { Size } from '@core/types';
import { selectTimeLine, TimeLineBlockState } from '@store/tasks';

import { getTimeLineOverflowBlockBackground } from './style';
import { timeLineWidth } from './style/time-line.style';
import { CurrentTimeIndicator } from './time-indicator';
import { TimeLineItem } from './time-line-item';

const MINUTE_MS = 60 * 1000;

function isFutureTimeLine(currentDate: Date, timeLine: TimeLineBlockState[]): boolean {
  const timeLineStartDateRaw = timeLine.at(0)?.startDate;
  const timeLineStartDate = timeLineStartDateRaw ? parseISO(timeLineStartDateRaw) : null;
  const isFuture = timeLineStartDate ? currentDate < timeLineStartDate : false;

  return isFuture;
}

function isPastTimeLine(currentDate: Date, timeLine: TimeLineBlockState[]): boolean {
  const timeLineEndDateRaw = timeLine.at(-1)?.endDate;
  const timeLineEnd = timeLineEndDateRaw ? parseISO(timeLineEndDateRaw) : null;
  const isPast = timeLineEnd ? currentDate > timeLineEnd : false;

  return isPast;
}

function getTimeIndicatorPosition(currentDate: Date, timeLine: TimeLineBlockState[]): Size {
  let currentTimePosition: Size = 0 as Size;

  for (const item of timeLine) {
    const { size, startDate: rawStartDate, endDate: rawEndDate, minuteSize } = item;

    const startDate = parseISO(rawStartDate);
    const endDate = parseISO(rawEndDate);

    if (currentDate >= startDate && currentDate < endDate) {
      currentTimePosition = (currentTimePosition +
        diffInMinutes(currentDate, startDate) * minuteSize) as Size;
      return currentTimePosition;
    }

    currentTimePosition = (currentTimePosition + size) as Size;
  }

  return currentTimePosition;
}

export function TimeLine() {
  const timeLine = useSelector(selectTimeLine, shallowEqual);

  const [isFuture, setIsFuture] = useState(false);
  const [isPast, setIsPast] = useState(false);
  const [currentTimePosition, setCurrentTimePosition] = useState(0 as Size);

  const refreshIndicator = () => {
    const date = new Date();
    const newIsFuture = isFutureTimeLine(date, timeLine);
    setIsFuture(newIsFuture);

    const newIsPast = isPastTimeLine(date, timeLine);
    setIsPast(newIsPast);

    if (newIsFuture) {
      setCurrentTimePosition(0 as Size);
      return;
    }

    const newCurrentTimePosition = getTimeIndicatorPosition(date, timeLine);

    setCurrentTimePosition(newCurrentTimePosition);
  };

  useEffect(() => {
    refreshIndicator();

    const interval = setInterval(() => {
      refreshIndicator();
    }, MINUTE_MS);

    return () => {
      clearInterval(interval);
    };
  }, [timeLine]);

  const items = timeLine.map((item, index) => {
    const { size, startTime, endTime, isBefore, isAfter } = item;

    const isLast = index === timeLine.length - 1;

    return (
      <TimeLineItem
        size={size}
        startTime={startTime}
        endTime={endTime}
        isBefore={isBefore}
        isAfter={isAfter}
        key={startTime}
        isLast={isLast}
      />
    );
  });

  return (
    <Flex
      width={timeLineWidth}
      minWidth={timeLineWidth}
      maxWidth={timeLineWidth}
      direction='column'
      sx={getTimeLineOverflowBlockBackground()}
    >
      {!isFuture && <CurrentTimeIndicator position={currentTimePosition} isPast={isPast} />}
      {items}
    </Flex>
  );
}
