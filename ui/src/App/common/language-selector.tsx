import { ComponentDefaultProps, Select } from '@chakra-ui/react';
import * as React from 'react';
import { useSelector } from 'react-redux';

import i18n from '@common/i18n';
import { selectCurrentUser } from '@store/session';

export function LanguageSelector(props: ComponentDefaultProps) {
  const user = useSelector(selectCurrentUser);
  const currentLang = (user ? user.language : i18n.language) || 'en';

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newLang = e.target.value;
    i18n.changeLanguage(newLang);
  };

  return (
    <Select
      value={currentLang}
      width='10rem'
      borderRadius='none'
      variant='flushed'
      {...props}
      onChange={handleChange}
    >
      <option value='en'>English</option>
      <option value='ru'>Russian</option>
    </Select>
  );
}
