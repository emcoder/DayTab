export enum RoutePath {
  Index = '/',
  Settings = '/settings',
  SignOut = '/sign-out',
}
