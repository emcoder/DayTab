import { extendTheme } from '@chakra-ui/react';
import * as React from 'react';

import { Size } from '@core/types';

export const smallScreenMediaQuery = '(max-width: 768px)';
export const primaryColorScheme = 'blue';

export const theme = extendTheme({
  fonts: {
    body: '"Fira Sans Condensed", serif, sans-serif',
  },
});

export const minimumTaskSize = <Size>60;
export const TIME_LINE_BLOCK_SIZE_MINIMUM = minimumTaskSize;

export function getBackgroundStyle(): React.CSSProperties {
  const bgGridColor = '#cbefff';
  const bgCellSize = '5px';

  return {
    backgroundColor: '#f7f7f7',
    backgroundImage:
      `repeating-linear-gradient(${bgGridColor} 0 1px, transparent 1px 100%),` +
      `repeating-linear-gradient(90deg, ${bgGridColor} 0 1px, transparent 1px 100%)`,
    backgroundSize: `${bgCellSize} ${bgCellSize}`,
  };
}

export const taskTextNormalColor = 'gray.600';
export const taskTextPastColor = 'gray.400';

export function getTaskTextColor(isPast: boolean) {
  return isPast ? taskTextPastColor : taskTextNormalColor;
}