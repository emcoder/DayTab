import { Flex } from '@chakra-ui/react';
import * as React from 'react';

export function BackgroundClickCatcher({ onClick }: { onClick: () => void }) {
  return <Flex w='100vw' h='100vh' position='fixed' top={0} left={0} onClick={onClick} />;
}
