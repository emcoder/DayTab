import { gql } from '@urql/core';

import { graphQLClient, GraphQLClient } from './graphql-client';

export interface AuthUser {
  id: number;
  username: string;
  timeZone: string;
  language: string;
}

export class AuthApi {
  constructor(private readonly client: GraphQLClient) {}

  async signIn(username: string, password: string): Promise<{ token: string }> {
    const query = gql<{ signIn: { token: string } }>`
      mutation signIn($username: String!, $password: String!) {
        signIn(username: $username, password: $password) {
          token
        }
      }
    `;

    const result = await this.client.mutation(query, { username, password });

    return result.signIn;
  }
}

export const authApi = new AuthApi(graphQLClient);
