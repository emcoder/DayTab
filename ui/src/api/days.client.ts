import { DayDate, DayId, Time, UserId } from '@daytab/common';
import { gql } from '@urql/core';
import { some, none, Option } from 'fp-ts/Option';

import { graphQLClient, GraphQLClient } from './graphql-client';

import type { FindOneDayParams, SaveDayParams } from '@core/days';

export interface Day {
  id: DayId;
  date: DayDate;
  startTime: Time;
  endTime: Time;
  userId: UserId;
}

export class DaysClient {
  constructor(private readonly client: GraphQLClient) {}

  async findOne(params: FindOneDayParams): Promise<Option<Day>> {
    const query = gql<{ day: Day }>`
      query day($userId: Int!, $date: String!) {
        day(userId: $userId, date: $date) {
          id
          date
          startTime
          endTime
          userId
        }
      }
    `;

    const { day } = await this.client.query(query, { ...params });

    return day ? some(day) : none;
  }

  async save(params: SaveDayParams): Promise<Day> {
    const query = gql<{ saveDay: Day }>`
      mutation saveDay($userId: Int!, $date: String!, $startTime: String!, $endTime: String!) {
        saveDay(userId: $userId, date: $date, startTime: $startTime, endTime: $endTime) {
          id
          date
          startTime
          endTime
          userId
        }
      }
    `;

    const { saveDay } = await this.client.mutation(query, {
      ...params,
    });

    return saveDay;
  }
}

export const daysClient = new DaysClient(graphQLClient);
