/* eslint-disable max-classes-per-file */
import { CombinedError } from '@urql/core';
import { GraphQLError } from 'graphql';
import { StatusCodes } from 'http-status-codes';

export class GraphQLClientError extends Error {
  readonly originalError: Error;

  constructor(err: Error) {
    super(`GraphQL client error: ${err.message}`);
    this.originalError = err;
  }
}

export class GraphQLQueryError extends Error {
  constructor(errors: GraphQLError[]) {
    super(`GraphQL query errors: ${errors.map((err) => err.message).join('; ')}`);
  }
}

export class GraphQLNetworkError extends Error {
  readonly originalError: CombinedError | null = null;

  readonly code: StatusCodes | null = null;

  constructor(code: StatusCodes, err?: CombinedError) {
    let message = `GraphQL request network error`;
    if (code) {
      message += ` (status = ${code})`;
    }

    if (err && err.message) {
      message += `: ${err.message}`;
    }

    super(message);

    this.code = code;
    this.originalError = err || null;
  }
}

export class GraphQLUnknownError extends Error {
  readonly originalError: CombinedError;

  constructor(err: CombinedError) {
    super(`Unknown GraphQL request error: ${err.message}`);
    this.originalError = err;
  }
}

export class UnauthorizedError extends Error {
  readonly code = StatusCodes.UNAUTHORIZED;

  constructor() {
    super('Unauthorized error');
  }
}

export class GraphQLNoDataError extends Error {
  constructor() {
    super('No data received from GraphQL API');
  }
}
