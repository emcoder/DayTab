import {
  createClient,
  Client,
  TypedDocumentNode,
  OperationResult,
  CombinedError,
} from '@urql/core';
import { GraphQLError } from 'graphql';
import { StatusCodes } from 'http-status-codes';

import { getToken } from '../common/auth';
import { logger } from '../common/logger';
import { API_URL } from '../config';

import {
  GraphQLClientError,
  GraphQLQueryError,
  GraphQLNetworkError,
  GraphQLUnknownError,
  UnauthorizedError,
  GraphQLNoDataError,
} from './errors';

export const gqlClient = createClient({
  url: API_URL,
  fetchOptions: () => {
    const token = getToken();
    return {
      headers: { authorization: token ? `Bearer ${token}` : '' },
    };
  },
});

export class GraphQLClient {
  private readonly client: Client;

  constructor() {
    this.client = createClient({
      url: API_URL,
      fetchOptions: () => {
        const token = getToken();
        return {
          headers: { authorization: token ? `Bearer ${token}` : '' },
        };
      },
    });
  }

  async query<T, V extends { [key: string]: unknown }>(
    query: TypedDocumentNode<T, V>,
    variables?: V,
  ): Promise<T> {
    let result: OperationResult<T, V>;
    try {
      result = await this.client.query(query, variables || <V>{}).toPromise();
    } catch (err) {
      this.handleClientError(<Error>err);
    }

    const { data, error } = result;

    if (error) {
      return this.handleRequestError(error);
    }

    if (!data) {
      throw new GraphQLNoDataError();
    }

    return data;
  }

  async mutation<T, V extends { [key: string]: unknown }>(
    query: TypedDocumentNode<T, V>,
    variables?: V,
  ): Promise<T> {
    let result: OperationResult<T, V>;
    try {
      result = await this.client.mutation(query, variables || <V>{}).toPromise();
    } catch (err) {
      return this.handleClientError(<Error>err);
    }

    const { data, error } = result;

    if (error) {
      return this.handleRequestError(error);
    }

    if (!data) {
      throw new GraphQLNoDataError();
    }

    return data;
  }

  private handleClientError(error: Error): never {
    logger.error(`GraphQL client error: ${error.message}`);
    throw new GraphQLClientError(error);
  }

  private handleRequestError(error: CombinedError): never {
    const { response, graphQLErrors } = error as {
      response: Response;
      graphQLErrors: GraphQLError[];
    };
    if (graphQLErrors.length > 0) {
      const isAuthorizationError = graphQLErrors.some(
        ({ message }) => message === 'Authorization failed',
      );
      if (isAuthorizationError) {
        throw new UnauthorizedError();
      } else {
        throw new GraphQLQueryError(graphQLErrors);
      }
    }
    if (response && response.status) {
      switch (response.status) {
        case StatusCodes.UNAUTHORIZED:
          throw new UnauthorizedError();
        default:
          throw new GraphQLNetworkError(response.status, error);
      }
    }

    throw new GraphQLUnknownError(error);
  }
}

export const graphQLClient = new GraphQLClient();
