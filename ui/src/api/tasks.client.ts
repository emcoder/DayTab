import { UserId } from '@daytab/common';
import { gql } from '@urql/core';
import { parseISO } from 'date-fns';

import {
  CreateTaskParams,
  DeleteTaskParams,
  FindAllTasksParams,
  Task,
  UpdateTaskParams,
} from '../core/tasks/types';

import { graphQLClient, GraphQLClient } from './graphql-client';

export interface GraphQLTask {
  id: number;
  name: string;
  start: string;
  end: string;
  userId: UserId;
}

export class TasksClient {
  constructor(private readonly client: GraphQLClient) {}

  async findAll(params: FindAllTasksParams): Promise<Task[]> {
    const { start, end } = params;

    const query = gql<{ tasks: GraphQLTask[] }>`
      query tasks($start: DateTime!, $end: DateTime!) {
        tasks(start: $start, end: $end) {
          id
          name
          start
          end
        }
      }
    `;

    const { tasks } = await this.client.query(query, {
      start: start.toISOString(),
      end: end.toISOString(),
    });

    return tasks.map((task) => this.parseResponseTask(task));
  }

  async create(params: CreateTaskParams): Promise<Task> {
    const { name, start, end } = params;

    const query = gql<{ createTask: GraphQLTask }>`
      mutation createTask($name: String!, $start: DateTime!, $end: DateTime!) {
        createTask(name: $name, start: $start, end: $end) {
          id
          name
          start
          end
          userId
        }
      }
    `;

    const { createTask } = await this.client.mutation(query, {
      name,
      start,
      end,
    });
    if (!createTask) {
      throw new Error('Empty response');
    }

    return this.parseResponseTask(createTask);
  }

  async update(params: UpdateTaskParams): Promise<Task> {
    const { id, name, start, end } = params;

    const query = gql<{ updateTask: GraphQLTask }>`
      mutation updateTask($id: Int!, $name: String, $start: DateTime, $end: DateTime) {
        updateTask(id: $id, name: $name, start: $start, end: $end) {
          id
          name
          start
          end
          userId
        }
      }
    `;

    const { updateTask } = await this.client.mutation(query, {
      id,
      name,
      start,
      end,
    });
    if (!updateTask) {
      throw new Error('Empty response');
    }

    return this.parseResponseTask(updateTask);
  }

  async delete(params: DeleteTaskParams): Promise<Task> {
    const { id } = params;

    const query = gql<{ deleteTask: GraphQLTask }>`
      mutation deleteTask($id: Int!) {
        deleteTask(id: $id) {
          id
          name
          start
          end
          userId
        }
      }
    `;

    const { deleteTask } = await this.client.mutation(query, {
      id,
    });
    if (!deleteTask) {
      throw new Error('Empty response');
    }

    return this.parseResponseTask(deleteTask);
  }

  private parseResponseTask(task: GraphQLTask): Task {
    return {
      ...task,
      start: parseISO(task.start),
      end: parseISO(task.end),
    };
  }
}

export const tasksClient = new TasksClient(graphQLClient);
