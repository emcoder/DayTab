import { TgChatId, TimeZone, UserId } from '@daytab/common';
import { gql } from '@urql/core';

import { getBrowserTimeZone } from '@common/helpers/time-zone.helpers';
import { User } from '@core/users';

import { graphQLClient, GraphQLClient } from './graphql-client';

export interface ApiUser {
  id: UserId;
  username: string;
  timeZone: TimeZone | null;
  language: string;
}

export class UsersClient {
  constructor(private readonly client: GraphQLClient) {}

  async findCurrentUser(): Promise<User> {
    const query = gql<{ user: ApiUser }>`
      query {
        user {
          id
          username
          timeZone
          language
        }
      }
    `;

    const { user } = await this.client.query(query, {});

    return {
      ...user,
      timeZone: user.timeZone || getBrowserTimeZone(),
    };
  }

  async userLinkToTelegram(tgChatId: TgChatId): Promise<void> {
    const query = gql<{ userLinkToTelegram: unknown }>`
      mutation userLinkToTelegram($tgChatId: Int!) {
        userLinkToTelegram(tgChatId: $tgChatId)
      }
    `;

    await this.client.mutation(query, { tgChatId });
  }
}

export const usersClient = new UsersClient(graphQLClient);
