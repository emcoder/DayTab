function setToken(token: string | null) {
  if (!token) {
    localStorage.removeItem('token');
    return;
  }
  localStorage.setItem('token', token);
}

function getToken(): string | null {
  return localStorage.getItem('token');
}

function isExpired(token: string): boolean {
  let data: { exp: number };
  try {
    const dataEncoded = token.split('.')[1];
    const dataStr = Buffer.from(dataEncoded, 'base64').toString('utf-8');
    data = <{ exp: number }>JSON.parse(dataStr);
  } catch (err) {
    throw new Error(`Failed to check expiration of invalid token: ${(err as Error).toString()}`);
  }

  const { exp } = data;

  if (!exp) {
    throw new Error('Invalid token data');
  }

  const now = Math.floor(Date.now() / 1000);
  return now > exp;
}

export { setToken, getToken, isExpired };
