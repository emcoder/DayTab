import { StatusCodes } from 'http-status-codes';

export class UnauthorizedError extends Error {
  readonly status = StatusCodes.UNAUTHORIZED;

  constructor() {
    super('Unauthorized');
  }
}
