/**
 * Checks if argument is array with one or more elements
 */
export function isNonEmptyArray<T>(array?: T[] | null): array is [T, ...T[]] {
  return Array.isArray(array) && array.length > 0;
}
