import { Color, ColorType } from '@core/types';

import { theme } from '../../App/style/common.style';

import { parseToType } from './type.helpers';

const colorCodes = [
  'blue',
  'red',
  'green',
  'yellow',
  'teal',
  'cyan',
  'orange',
  'purple',
  'pink',
] as const;
const colorTones = ['100', '200', '300', '400'];

function getRandomInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min) + min);
}

export function getRandomColor(): Color {
  const colorIndex = getRandomInt(0, colorCodes.length);
  const toneIndex = getRandomInt(0, colorTones.length);

  const colorCode = colorCodes[colorIndex];
  const colorTone = colorTones[toneIndex];
  const colors = <Record<string, Record<string, string>>>theme.colors;
  const hex = colors[colorCode][colorTone];
  return parseToType(ColorType, hex);
}
