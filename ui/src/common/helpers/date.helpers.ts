import { DayDate, Time, TimeZone } from '@daytab/common';
import {
  addDays,
  addHours,
  addMinutes,
  intervalToDuration,
  parseISO,
  setMinutes,
  subDays,
} from 'date-fns';
import { format, formatInTimeZone, toDate, zonedTimeToUtc } from 'date-fns-tz';

import { Minutes } from '@core/types';

import { DATE_FORMAT, TIME_FORMAT } from '../constants/date.constants';

import { getBrowserTimeZone } from './time-zone.helpers';
import { diffInMinutes } from './time.helpers';

export function getDayDateForDate(date: Date, timeZone?: TimeZone): DayDate {
  const tz = timeZone || getBrowserTimeZone();

  return <DayDate>formatInTimeZone(date, tz, DATE_FORMAT);
}

export function getDateForDayDate(dayDate: DayDate, timeZone?: TimeZone): Date {
  const tz = timeZone || getBrowserTimeZone();
  return zonedTimeToUtc(parseISO(dayDate), tz);
}

export function getTime(date: Date, tz?: TimeZone): Time {
  return <Time>(tz ? formatInTimeZone(date, tz, TIME_FORMAT) : format(date, TIME_FORMAT));
}

export function getCurrentDayDate(tz?: TimeZone): DayDate {
  const date = new Date();
  if (tz) {
    return <DayDate>formatInTimeZone(date, tz, DATE_FORMAT);
  }
  return <DayDate>format(date, DATE_FORMAT);
}

export function getDateForTime(time: Time, date?: DayDate, tz?: TimeZone): Date {
  const timeDate = date || getCurrentDayDate(tz);

  const src = `${timeDate}T${time}`;
  return tz ? toDate(src, { timeZone: tz }) : toDate(src);
}

export function generateTimeLine(start: Time, end: Time): Time[] {
  let iterDate = getDateForTime(start);
  const endDate = getDateForTime(end);

  const result: Time[] = [];

  while (iterDate < endDate) {
    result.push(getTime(iterDate));
    iterDate = setMinutes(addHours(iterDate, 1), 0);
  }

  result.push(getTime(endDate));

  return result;
}

export function addMinutesToTime(time: Time, minutes: Minutes): Time {
  const date = getDateForTime(time);
  const result = addMinutes(date, minutes);
  return getTime(result);
}

export function getCurrentTime(tz?: TimeZone): Time {
  return getTime(new Date(), tz);
}

export function formatDateDiff(start: Date, end: Date): string {
  const duration = intervalToDuration({
    start,
    end,
  });

  const { hours, minutes } = duration;

  const result: string[] = [];
  if (hours && hours > 0) {
    result.push(`${hours}h`);
  }
  if (minutes && minutes > 0) {
    result.push(`${minutes}m`);
  }

  return result.join(' ');
}

export function intersectionInMinutes(from1: Date, to1: Date, from2: Date, to2: Date): Minutes {
  if (from2 >= from1 && to2 <= to1) {
    return diffInMinutes(to2, from2);
  }
  if (from2 >= from1 && from2 < to1) {
    return diffInMinutes(to1, from2);
  }
  if (to2 > from1 && to2 <= to1) {
    return diffInMinutes(to2, from1);
  }
  if (from2 < from1 && to2 > to1) {
    return diffInMinutes(to1, from1);
  }
  return <Minutes>0;
}

export function getDateForDateAndTime(date: DayDate, time: Time, tz: TimeZone): Date {
  return zonedTimeToUtc(`${date}T${time}`, tz);
}

export function isValidDateString(date: string): boolean {
  const parsedDate = parseISO(date);
  return parsedDate.toString() !== 'Invalid Date';
}

export function getMaxDate(...dates: Date[]): Date {
  let maxDate = dates[0];

  for (let i = 1; i < dates.length; i++) {
    const date = dates[i];
    if (date > maxDate) {
      maxDate = date;
    }
  }

  return maxDate;
}

export function getMinDate(...dates: Date[]): Date {
  let minDate = dates[0];

  for (let i = 1; i < dates.length; i++) {
    const date = dates[i];
    if (date < minDate) {
      minDate = date;
    }
  }

  return minDate;
}

export function getPrevDayDate(dayDate: DayDate, timeZone?: TimeZone): DayDate {
  const currDate = getDateForDayDate(dayDate);
  const prevDate = subDays(currDate, 1);
  return getDayDateForDate(prevDate, timeZone);
}

export function getNextDayDate(dayDate: DayDate, timeZone?: TimeZone): DayDate {
  const currDate = getDateForDayDate(dayDate);
  const nextDate = addDays(currDate, 1);
  return getDayDateForDate(nextDate, timeZone);
}

export function getDayEndDate(dayDate: DayDate, endTime: Time, timeZone: TimeZone): Date {
  const nextDayDate = getNextDayDate(dayDate, timeZone);
  const date = getDateForDateAndTime(nextDayDate, endTime, timeZone);
  return date;
}
