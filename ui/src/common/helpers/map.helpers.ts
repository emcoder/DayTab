// eslint-disable-next-line
export function mapBy<T, K extends keyof T>(entities: T[], column: K): Map<T[K], T> {
  return entities.reduce((map, entity) => {
    map.set(entity[column], entity);
    return map;
  }, new Map<T[K], T>());
}

export function mapAndGroupBy<T, K extends keyof T>(entities: T[], column: K): Map<T[K], T[]> {
  return entities.reduce((map, entity) => {
    const key = entity[column];
    const arr = map.get(key) || [];
    arr.push(entity);
    map.set(key, arr);

    return map;
  }, new Map<T[K], T[]>());
}
