import { Minutes, Size, SizeType } from '@core/types';

import { parseToType } from './type.helpers';

export function minutesToSize(minutes: Minutes, minuteSize: Size): Size {
  return parseToType(SizeType, Math.ceil(minutes * minuteSize));
}
