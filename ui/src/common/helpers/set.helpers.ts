export function collectPropertyValues<T, K extends keyof T>(
  entities: T[],
  property: K,
  options: {
    excludeFalsy: true;
  },
): Set<NonNullable<T[K]>>;
export function collectPropertyValues<T, K extends keyof T>(
  entities: T[],
  property: K,
  options: {
    excludeNullish: true;
  },
): Set<NonNullable<T[K]>>;
export function collectPropertyValues<T, K extends keyof T>(entities: T[], property: K): Set<T[K]>;
export function collectPropertyValues<T, K extends keyof T>(
  entities: T[],
  property: K,
  options?: {
    /**
     * @default false
     */
    excludeNullish?: boolean;
    /**
     * @default false
     */
    excludeFalsy?: boolean;
  },
): Set<T[K]> {
  return entities.reduce((set, entity) => {
    const { excludeNullish = false, excludeFalsy = false } = options || {};

    const value = entity[property];

    const allowValue =
      (excludeFalsy ? Boolean(value) : true) &&
      (excludeNullish ? value !== null && value !== undefined : true);

    if (allowValue) {
      set.add(value);
    }

    return set;
  }, new Set<T[K]>());
}
