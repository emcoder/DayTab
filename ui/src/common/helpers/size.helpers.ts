import { Size } from '@core/types';

export function formatSize(size: Size): `${number}px` {
  return `${size}px`;
}
