import { TimeZone } from '@daytab/common';

export function getBrowserTimeZone(): TimeZone {
  return <TimeZone>(Intl.DateTimeFormat().resolvedOptions().timeZone || 'UTC');
}
