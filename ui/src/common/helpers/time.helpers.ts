import { differenceInMinutes } from 'date-fns';

import { Minutes, MinutesType, Size } from '@core/types';

import { parseToType } from './type.helpers';

export function diffInMinutes(endDate: Date, startDate: Date): Minutes {
  return parseToType(MinutesType, differenceInMinutes(endDate, startDate));
}

export function getMinuteSize(size: Size, minutes: Minutes): Size {
  return <Size>Math.ceil(size / minutes);
}
