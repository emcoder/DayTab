import { isLeft } from 'fp-ts/lib/Either';
import * as t from 'io-ts';

export function parseToType<T extends t.Any>(type: T, value: t.InputOf<T>): t.TypeOf<T> {
  const either = type.decode(value);
  if (isLeft(either)) {
    throw new Error(either.left.map((err) => err.message).join('; '));
  }
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return either.right;
}
