/**
 * Prepare timezone code before display, remove underscores, etc
 */
export function formatTimezone(timezone: string): string {
  return timezone.replace('_', ' ');
}

const timezones = ['UTC', 'Europe/Moscow', 'America/New_York'];

export default timezones;
