export type ActionType<T extends string, F extends (...args: unknown[]) => unknown> = {
  type: T;
} & ReturnType<F>;

export type WithOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
export type WithRequired<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;
