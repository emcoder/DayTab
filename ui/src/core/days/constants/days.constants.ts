import { Time } from '@daytab/common';

export const DEFAULT_DAY_END_TIME: Time = <Time>'04:00';

export const CURRENT_DAY_DATE_LS_TOKEN = 'currentDayDate';
