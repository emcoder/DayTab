import { DayDate, DayDateType, Time, TimeZone, UserId } from '@daytab/common';
import { isLeft } from 'fp-ts/Either';
import { Option } from 'fp-ts/Option';

import { daysClient as client, DaysClient } from '@api/days.client';
import { getCurrentDayDate } from '@common/helpers/date.helpers';
import { logger } from '@common/logger';

import { CURRENT_DAY_DATE_LS_TOKEN } from './constants';
import { Day, FindOneDayParams, SaveDayParams } from './types';

export class DaysService {
  constructor(private readonly daysClient: DaysClient) {}

  async save(params: SaveDayParams): Promise<Day> {
    const day = await this.daysClient.save(params);
    return day;
  }

  initDay(userId: UserId, date: DayDate): Day {
    return {
      date,
      userId,
      ...this.getDefaultDayDuration(),
    };
  }

  createCurrentDay(userId: UserId, timeZone: TimeZone): Day {
    const date = getCurrentDayDate(timeZone);

    return {
      date,
      userId,
      ...this.getDefaultDayDuration(),
    };
  }

  getDefaultDayDuration(): Pick<Day, 'startTime' | 'endTime'> {
    return {
      startTime: <Time>'08:00',
      endTime: <Time>'23:00',
    };
  }

  findCurrentDayDate(timeZone: TimeZone): DayDate {
    const lsCurrentDayDate = localStorage.getItem(CURRENT_DAY_DATE_LS_TOKEN);

    if (!lsCurrentDayDate) {
      return getCurrentDayDate(timeZone);
    }

    const parseResult = DayDateType.decode(lsCurrentDayDate);
    if (isLeft(parseResult)) {
      logger.warn(`Failed to parse current day date "${lsCurrentDayDate}"`);
      return getCurrentDayDate(timeZone);
    }
    const currentDayDate = parseResult.right;

    return currentDayDate;
  }

  setCurrentDayDate(date: DayDate) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    localStorage.setItem(CURRENT_DAY_DATE_LS_TOKEN, date);
  }

  async findOne(params: FindOneDayParams): Promise<Option<Day>> {
    return this.daysClient.findOne(params);
  }
}

export const daysService = new DaysService(client);
