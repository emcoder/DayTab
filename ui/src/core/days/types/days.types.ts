import { DayDate, Time, UserId } from '@daytab/common';

export interface Day {
  date: DayDate;
  startTime: Time;
  endTime: Time;
  userId: UserId;
}

export interface SaveDayParams {
  date: DayDate;
  startTime: Time;
  endTime: Time;
  userId: UserId;
}

export interface FindOneDayParams {
  userId: UserId;
  date: DayDate;
}
