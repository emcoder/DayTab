import { DayDate, Time, TimeZone } from '@daytab/common';
import { addDays, getHours } from 'date-fns';
import { utcToZonedTime } from 'date-fns-tz';

import { getDateForDateAndTime, getDayEndDate } from '@common/helpers/date.helpers';

export function getTaskStartEnd(params: {
  dayDate: DayDate;
  startTime: Time;
  endTime: Time;
  dayEndTime: Time;
  timeZone: TimeZone;
}): {
  start: Date;
  end: Date;
} {
  const { dayDate, startTime, endTime, dayEndTime, timeZone } = params;

  const dayEndDate = getDayEndDate(dayDate, dayEndTime, timeZone);
  const dayEndHours = getHours(utcToZonedTime(dayEndDate, timeZone));

  let start = getDateForDateAndTime(dayDate, startTime, timeZone);
  let end = getDateForDateAndTime(dayDate, endTime, timeZone);

  const startHours = getHours(utcToZonedTime(start, timeZone));
  if (startHours < dayEndHours) {
    // We should support time like 01:00 at current day,
    // while in fact it's already the next day from the user's point of view
    start = addDays(start, 1);
    end = addDays(end, 1);
  }

  if (end < start) {
    end = addDays(end, 1);
  }

  return {
    start,
    end,
  };
}
