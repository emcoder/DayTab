import { IsoDate } from '@daytab/common';

import { getRandomColor } from '@common/helpers/color.helpers';
import { intersectionInMinutes } from '@common/helpers/date.helpers';
import { minutesToSize } from '@common/helpers/minutes.helpers';
import { diffInMinutes } from '@common/helpers/time.helpers';
import { logger } from '@common/logger';
import { TimeLine, TimeLineBlock } from '@core/time-line';
import { DEFAULT_TASK_SIZE_MINIMUM } from '@core/time-line/constants';
import { Minutes, Size } from '@core/types';
import { TaskState } from '@store/tasks/tasks.types';

import {
  Task,
  TasksGenerateSizesMapParams,
  TaskSizesMap,
  TaskListSector,
  TaskForSizesMap,
  TaskSizeAndLevelData,
} from './types';

interface TimeLineBlockTaskListSector {
  taskId: Task['id'];
  startDate: Date;
  endDate: Date;
  minutes: Minutes;
  size: Size;
}

export class TasksDisplayService {
  getTasksState(tasks: Task[], timeLine: TimeLine): TaskState[] {
    const tasksLevels = this.groupTasksByLevels(tasks);

    const taskSizesAndLevelsMap = new Map<Task['id'], TaskSizeAndLevelData>();
    for (let i = 0; i < tasksLevels.length; i++) {
      const tasksLevel = tasksLevels[i];

      let taskSizesMap: TaskSizesMap;
      try {
        taskSizesMap = this.generateSizesMap({
          tasks: tasksLevel,
          timeLine,
        });
      } catch (err) {
        const error = `Failed to generate tasks sizes map: ${(<Error>err).message}`;
        logger.error(error);
        throw new Error(error);
      }

      for (const [id, { size, offset }] of taskSizesMap.entries()) {
        taskSizesAndLevelsMap.set(id, {
          size,
          offset,
          level: i,
        });
      }
    }

    const tasksState = tasks.reduce((arr, task) => {
      const { id, start, end } = task;
      const taskSizeMapping = taskSizesAndLevelsMap.get(id);
      const { size, offset, level } = taskSizeMapping || {
        size: DEFAULT_TASK_SIZE_MINIMUM,
        offset: <Size>0,
        level: 0,
      };

      arr.push({
        ...task,
        start: <IsoDate>start.toISOString(),
        end: <IsoDate>end.toISOString(),
        color: getRandomColor(),
        size,
        offset,
        level,
      });
      return arr;
    }, <TaskState[]>[]);

    return tasksState;
  }

  private generateSizesMap(params: TasksGenerateSizesMapParams): TaskSizesMap {
    const { timeLine, tasks } = params;

    const taskSectors: TaskListSector[] = [];

    for (const timeLineBlock of timeLine) {
      const { size: blockSize } = timeLineBlock;

      const blockTasks = this.findBlockTasks(timeLineBlock, tasks);

      if (blockTasks.length === 0) {
        taskSectors.push({
          taskId: null,
          size: blockSize,
        });
        continue;
      }

      const blockTaskListSectors: TimeLineBlockTaskListSector[] = [];

      for (const task of blockTasks) {
        const sector = this.getBlockTaskSector({
          task,
          block: timeLineBlock,
        });

        blockTaskListSectors.push(sector);
      }

      const allTaskListSectors = this.getTaskListSectors({
        block: timeLineBlock,
        blockTaskListSectors,
      });
      taskSectors.push(...allTaskListSectors);
    }

    const taskSizesMap = this.getTaskSizesMapFromSectors(taskSectors);
    return taskSizesMap;
  }

  private groupTasksByLevels(tasks: Task[]): Task[][] {
    const taskLevels: Task[][] = [[]];

    const addedMap = new Map<number, Task>();
    for (const task of tasks) {
      const [primaryLevel] = taskLevels;

      if (addedMap.has(task.id)) {
        continue;
      }

      primaryLevel.push(task);
      addedMap.set(task.id, task);

      const overlapTasks = this.findOverlappingTasks(tasks, task);
      for (let i = 0; i < overlapTasks.length; i++) {
        const overlapTask = overlapTasks[i];
        const { id: overlapId } = overlapTask;

        if (addedMap.has(overlapId)) {
          continue;
        }

        const levelIndex = i + 1;
        const arr = taskLevels[levelIndex] || [];
        arr.push(overlapTask);
        taskLevels[levelIndex] = arr;

        addedMap.set(overlapId, overlapTask);
      }
    }

    return taskLevels;
  }

  private findOverlappingTasks(tasks: Task[], task: Task): Task[] {
    const result: Task[] = [];

    const { id, start, end } = task;

    for (const otherTask of tasks) {
      const { id: otherId, start: otherStart, end: otherEnd } = otherTask;
      if (otherId !== id && intersectionInMinutes(start, end, otherStart, otherEnd) > 0) {
        result.push(otherTask);
      }
    }

    return result;
  }

  private getBlockTaskSector(params: {
    task: TaskForSizesMap;
    block: TimeLineBlock;
  }): TimeLineBlockTaskListSector {
    const { task, block } = params;
    const { id: taskId, start: taskStartDate, end: taskEndDate } = task;
    const {
      startDate: blockStartDate,
      endDate: blockEndDate,
      minutes: blockMinutes,
      minuteSize,
      size: blockSize,
    } = block;

    const taskMinutes = diffInMinutes(taskEndDate, taskStartDate);

    if (taskStartDate >= blockStartDate) {
      const sectorEndDate = taskEndDate < blockEndDate ? taskEndDate : blockEndDate;
      const minutes =
        taskEndDate < blockEndDate ? taskMinutes : diffInMinutes(blockEndDate, taskStartDate);
      return {
        taskId,
        startDate: taskStartDate,
        endDate: sectorEndDate,
        minutes,
        size: minutesToSize(minutes, minuteSize),
      };
    }
    if (taskEndDate > blockStartDate && taskEndDate <= blockEndDate) {
      const minutes = diffInMinutes(taskEndDate, blockStartDate);
      return {
        taskId,
        startDate: blockStartDate,
        endDate: taskEndDate,
        minutes,
        size: minutesToSize(minutes, minuteSize),
      };
    }
    return {
      taskId,
      startDate: blockStartDate,
      endDate: blockEndDate,
      minutes: blockMinutes,
      size: blockSize,
    };
  }

  private findBlockTasks(
    block: Pick<TimeLineBlock, 'startDate' | 'endDate'>,
    tasks: TaskForSizesMap[],
  ): TaskForSizesMap[] {
    const { startDate: blockStartDate, endDate: blockEndDate } = block;

    const blockTasks = tasks.filter(
      ({ start: taskStartDate, end: taskEndDate }) =>
        intersectionInMinutes(blockStartDate, blockEndDate, taskStartDate, taskEndDate) > 0,
    );

    return blockTasks;
  }

  private getTaskListSectors(params: {
    block: TimeLineBlock;
    blockTaskListSectors: TimeLineBlockTaskListSector[];
  }): TaskListSector[] {
    const { block, blockTaskListSectors } = params;

    const { startDate: blockStartDate, endDate: blockEndDate, minuteSize } = block;

    const taskSectors: TaskListSector[] = [];

    for (let j = 0; j <= blockTaskListSectors.length - 1; j++) {
      const currentSector = blockTaskListSectors[j];
      const nextSector = blockTaskListSectors[j + 1];

      const {
        startDate: currentSectorStartDate,
        endDate: currentSectorEndDate,
        taskId,
        size,
      } = currentSector;

      // In first sector, we need to check the space before it
      if (j === 0) {
        const minutesBefore = diffInMinutes(currentSectorStartDate, blockStartDate);
        const sizeBefore = minutesToSize(minutesBefore, minuteSize);
        if (sizeBefore > 0) {
          taskSectors.push({
            taskId: null,
            size: sizeBefore,
          });
        }
      }

      taskSectors.push({
        taskId,
        size,
      });

      if (nextSector) {
        const { startDate: nextSectorStartDate } = nextSector;

        const minutesBetween = diffInMinutes(nextSectorStartDate, currentSectorEndDate);
        const sizeBetween = minutesToSize(minutesBetween, minuteSize);
        taskSectors.push({
          taskId: null,
          size: sizeBetween,
        });
      } else {
        const minutesAfter = diffInMinutes(blockEndDate, currentSectorEndDate);
        const sizeAfter = minutesToSize(minutesAfter, minuteSize);
        if (sizeAfter > 0) {
          taskSectors.push({
            taskId: null,
            size: sizeAfter,
          });
        }
      }
    }

    return taskSectors;
  }

  private getTaskSizesMapFromSectors(taskSectors: TaskListSector[]): TaskSizesMap {
    const taskSizesMap: TaskSizesMap = new Map();

    let offset: Size = <Size>0;
    for (const { taskId, size } of taskSectors) {
      if (taskId) {
        const taskSizeMapping = taskSizesMap.get(taskId) || {
          size: <Size>0,
          offset,
        };
        taskSizeMapping.size = <Size>(taskSizeMapping.size + size);
        taskSizesMap.set(taskId, taskSizeMapping);
        offset = <Size>0;
      } else {
        offset = <Size>(offset + size);
      }
    }

    return taskSizesMap;
  }
}

export const tasksDisplayService = new TasksDisplayService();
