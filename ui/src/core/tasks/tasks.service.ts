import { tasksClient, TasksClient } from '../../api';

import {
  CreateTaskParams,
  DeleteTaskParams,
  FindAllTasksParams,
  Task,
  UpdateTaskParams,
} from './types';

export class TasksService {
  constructor(private readonly tasksApi: TasksClient) {}

  async create(params: CreateTaskParams): Promise<Task> {
    const result = await this.tasksApi.create(params);
    return result;
  }

  async update(params: UpdateTaskParams): Promise<Task> {
    const result = await this.tasksApi.update(params);
    return result;
  }

  async find(params: FindAllTasksParams): Promise<Task[]> {
    const result = await this.tasksApi.findAll(params);
    return result;
  }

  async delete(params: DeleteTaskParams): Promise<Task> {
    const result = await this.tasksApi.delete(params);
    return result;
  }
}

export const tasksService = new TasksService(tasksClient);
