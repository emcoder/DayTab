import type { Task } from './tasks.types';
import type { TimeLineBlock } from '@core/time-line';
import type { Size } from '@core/types';

export type TaskForSizesMap = Pick<Task, 'id' | 'start' | 'end'>;

export interface TasksGenerateSizesMapParams {
  tasks: TaskForSizesMap[];
  timeLine: TimeLineBlock[];
}

export interface TaskListSector {
  taskId: number | null;
  size: Size;
}

export interface TaskSizeData {
  offset: Size;
  size: Size;
}

export interface TaskSizeAndLevelData {
  offset: Size;
  size: Size;
  level: number;
}

export type TaskSizesMap = Map<Task['id'], TaskSizeData>;
