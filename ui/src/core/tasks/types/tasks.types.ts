import { UserId } from '@daytab/common';

export interface FindAllTasksParams {
  start: Date;
  end: Date;
}

export interface CreateTaskParams {
  name: string;
  start: Date;
  end: Date;
}

export interface UpdateTaskParams {
  id: Task['id'];
  name: string;
  start: Date;
  end: Date;
}

export interface Task {
  id: number;
  name: string;
  start: Date;
  end: Date;
  userId: UserId;
}

export interface DeleteTaskParams {
  id: Task['id'];
}
