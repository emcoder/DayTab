import { Size } from '@core/types';

export const DEFAULT_BLOCK_SIZE_MINIMUM: Size = <Size>60;
export const DEFAULT_TASK_SIZE_MINIMUM: Size = <Size>60;
export const DEFAULT_MINUTE_SIZE: Size = <Size>1;
