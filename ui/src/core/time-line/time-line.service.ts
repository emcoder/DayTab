import { DayDate, IsoDate, Time, TimeZone } from '@daytab/common';
import { addHours, getMinutes, setMinutes } from 'date-fns';

import {
  getDateForDateAndTime,
  getDayEndDate,
  getMaxDate,
  getMinDate,
  getNextDayDate,
  getTime,
  intersectionInMinutes,
} from '@common/helpers/date.helpers';
import { minutesToSize } from '@common/helpers/minutes.helpers';
import { diffInMinutes, getMinuteSize } from '@common/helpers/time.helpers';

import {
  DEFAULT_BLOCK_SIZE_MINIMUM,
  DEFAULT_MINUTE_SIZE,
  DEFAULT_TASK_SIZE_MINIMUM,
} from './constants';
import {
  BaseTimeLineBlock,
  TaskForTimeLine,
  TimeLine,
  TimeLineBlock,
  TimeLineBlockInterval,
  TimeLineGenerateParams,
} from './types';

import type { Minutes, Size } from '@core/types';
import type { TimeLineBlockState } from '@store/tasks';

function findTasksBeforeTimeLine(
  timeLineStartDate: Date,
  tasks: TaskForTimeLine[],
): TaskForTimeLine[] {
  return tasks.filter(({ start }) => start < timeLineStartDate);
}

function findTasksAfterTimeLine(
  timeLineEndDate: Date,
  tasks: TaskForTimeLine[],
): TaskForTimeLine[] {
  return tasks.filter(({ end }) => end > timeLineEndDate);
}

export class TimeLineService {
  toState(timeLine: TimeLine): TimeLineBlockState[] {
    return timeLine.map((block) => ({
      ...block,
      startDate: <IsoDate>block.startDate.toISOString(),
      endDate: <IsoDate>block.endDate.toISOString(),
    }));
  }

  generate(params: TimeLineGenerateParams): TimeLineBlock[] {
    const {
      baseMinuteSize: minuteSize = DEFAULT_MINUTE_SIZE,
      tasks,
      taskSizeMinimum = DEFAULT_TASK_SIZE_MINIMUM,
      day,
      timeZone,
      dayEndTime,
      interval = TimeLineBlockInterval.Hour,
      blockSizeMinimum = DEFAULT_BLOCK_SIZE_MINIMUM,
    } = params;

    const { date, startTime: currDayStartTime, endTime: currDayEndTime } = day;

    const timeLineBlocks = this.generateBlocks({
      date,
      startTime: currDayStartTime,
      endTime: currDayEndTime,
      interval,
      timeZone,
      tasks,
      taskSizeMinimum,
      baseMinuteSize: minuteSize,
      blockSizeMinimum,
      dayEndTime,
    });

    const firstTimeLineBlock = timeLineBlocks[0];
    const lastTimeLineBlock = timeLineBlocks[timeLineBlocks.length - 1];

    const baseTimeLineStartDate = firstTimeLineBlock.startDate;
    const baseTimeLineEndDate = lastTimeLineBlock.endDate;

    const beforeBlock = this.getBeforeBlock({
      baseTimeLineStartDate,
      tasks,
      timeZone,
      taskSizeMinimum,
      baseMinuteSize: minuteSize,
      blockSizeMinimum,
      dayEndTime,
      date,
    });
    if (beforeBlock) {
      timeLineBlocks.unshift(beforeBlock);
    }

    const afterBlock = this.getAfterBlock({
      baseTimeLineEndDate,
      tasks,
      timeZone,
      taskSizeMinimum,
      baseMinuteSize: minuteSize,
      blockSizeMinimum,
      dayEndTime,
      date,
    });
    if (afterBlock) {
      timeLineBlocks.push(afterBlock);
    }

    return timeLineBlocks;
  }

  private getTaskMinuteSize(params: {
    task: TaskForTimeLine;
    baseMinuteSize: Size;
    taskSizeMinimum: Size;
  }): Size {
    const {
      task: { start: taskStart, end: taskEnd },
      baseMinuteSize,
      taskSizeMinimum,
    } = params;

    const taskMinutes = diffInMinutes(taskEnd, taskStart);

    const taskRawSize = minutesToSize(taskMinutes, baseMinuteSize);
    const taskRawSizeLessThanMinimum = taskRawSize < taskSizeMinimum;
    const taskSize = taskRawSizeLessThanMinimum ? taskSizeMinimum : taskRawSize;

    const taskMinuteSize = taskRawSizeLessThanMinimum
      ? getMinuteSize(taskSize, taskMinutes)
      : baseMinuteSize;

    return taskMinuteSize;
  }

  private getBlockSize(params: {
    blockMinutes: Minutes;
    blockTasks: TaskForTimeLine[];
    taskSizeMinimum: Size;
    baseMinuteSize: Size;
    blockSizeMinimum: Size;
  }): {
    size: Size;
    minuteSize: Size;
  } {
    const { blockMinutes, blockTasks, taskSizeMinimum, baseMinuteSize, blockSizeMinimum } = params;

    let resultMinuteSize: Size = baseMinuteSize;

    if (blockTasks.length > 0) {
      const taskMinuteSizes: Size[] = [];
      for (const task of blockTasks) {
        const taskMinuteSize = this.getTaskMinuteSize({
          task,
          taskSizeMinimum,
          baseMinuteSize,
        });
        taskMinuteSizes.push(taskMinuteSize);
      }

      const tasksMinuteSize = <Size>Math.max(...taskMinuteSizes);
      resultMinuteSize = tasksMinuteSize;
    }

    const blockSize = minutesToSize(blockMinutes, resultMinuteSize);

    const finalBlockSize = blockSize >= blockSizeMinimum ? blockSize : blockSizeMinimum;
    const finalMinuteSize = getMinuteSize(finalBlockSize, blockMinutes);

    return {
      size: finalBlockSize,
      minuteSize: finalMinuteSize,
    };
  }

  private getAfterBlock(params: {
    baseTimeLineEndDate: Date;
    tasks: TaskForTimeLine[];
    timeZone: TimeZone;
    blockSizeMinimum: Size;
    taskSizeMinimum: Size;
    baseMinuteSize: Size;
    dayEndTime: Time;
    date: DayDate;
  }): TimeLineBlock | null {
    const {
      baseTimeLineEndDate,
      tasks,
      timeZone,
      baseMinuteSize,
      taskSizeMinimum,
      blockSizeMinimum,
      dayEndTime,
      date,
    } = params;

    const tasksAfterTimeLine = findTasksAfterTimeLine(baseTimeLineEndDate, tasks);
    if (tasksAfterTimeLine.length === 0) {
      return null;
    }

    const tasksAfterTimeLineEndMax = getMaxDate(...tasksAfterTimeLine.map(({ end }) => end));
    const dayEndDate = getDayEndDate(date, dayEndTime, timeZone);

    const blockEndDate =
      tasksAfterTimeLineEndMax < dayEndDate ? tasksAfterTimeLineEndMax : dayEndDate;
    const afterBlockEndTime = getTime(blockEndDate, timeZone);

    const minutes = diffInMinutes(blockEndDate, baseTimeLineEndDate);

    const { size, minuteSize } = this.getBlockSize({
      blockTasks: tasksAfterTimeLine,
      blockMinutes: minutes,
      taskSizeMinimum,
      baseMinuteSize,
      blockSizeMinimum,
    });

    const afterBlock: TimeLineBlock = {
      startTime: getTime(baseTimeLineEndDate, timeZone),
      startDate: baseTimeLineEndDate,
      endTime: afterBlockEndTime,
      endDate: blockEndDate,
      minutes,
      isBefore: false,
      isAfter: true,
      size,
      minuteSize,
    };
    return afterBlock;
  }

  private getBeforeBlock(params: {
    baseTimeLineStartDate: Date;
    tasks: TaskForTimeLine[];
    timeZone: TimeZone;
    blockSizeMinimum: Size;
    taskSizeMinimum: Size;
    baseMinuteSize: Size;
    dayEndTime: Time;
    date: DayDate;
  }): TimeLineBlock | null {
    const {
      baseTimeLineStartDate,
      tasks,
      timeZone,
      taskSizeMinimum,
      baseMinuteSize,
      blockSizeMinimum,
      dayEndTime,
      date,
    } = params;
    const tasksBeforeTimeLine = findTasksBeforeTimeLine(baseTimeLineStartDate, tasks);

    if (tasksBeforeTimeLine.length === 0) {
      return null;
    }

    const tasksBeforeTimeLineStartMin = getMinDate(
      ...tasksBeforeTimeLine.map(({ start }) => start),
    );

    const prevDayEndDate = getDateForDateAndTime(date, dayEndTime, timeZone);
    const blockStartDate =
      tasksBeforeTimeLineStartMin > prevDayEndDate ? tasksBeforeTimeLineStartMin : prevDayEndDate;

    const beforeBlockStartTime = getTime(blockStartDate, timeZone);

    const minutes = diffInMinutes(baseTimeLineStartDate, blockStartDate);

    const { size, minuteSize } = this.getBlockSize({
      blockTasks: tasksBeforeTimeLine,
      blockMinutes: minutes,
      taskSizeMinimum,
      baseMinuteSize,
      blockSizeMinimum,
    });

    const beforeBlock: TimeLineBlock = {
      startTime: beforeBlockStartTime,
      startDate: blockStartDate,
      endTime: getTime(baseTimeLineStartDate, timeZone),
      endDate: baseTimeLineStartDate,
      minutes,
      isBefore: true,
      isAfter: false,
      size,
      minuteSize,
    };
    return beforeBlock;
  }

  private generateBlocks(params: {
    date: DayDate;
    startTime: Time;
    endTime: Time;
    interval: TimeLineBlockInterval;
    timeZone: TimeZone;
    tasks: TaskForTimeLine[];
    baseMinuteSize: Size;
    dayEndTime: Time;
    blockSizeMinimum: Size;
    taskSizeMinimum: Size;
  }): TimeLineBlock[] {
    const {
      date,
      startTime: start,
      endTime: end,
      interval,
      timeZone: tz,
      tasks,
      taskSizeMinimum,
      baseMinuteSize,
      blockSizeMinimum,
    } = params;

    let startDate = getDateForDateAndTime(date, start, tz);
    let endDate = getDateForDateAndTime(date, end, tz);

    if (endDate < startDate) {
      // This means that time line range ends on the next day
      const nextDayDate = getNextDayDate(date);
      endDate = getDateForDateAndTime(nextDayDate, end, tz);
    }

    const result: TimeLineBlock[] = [];

    while (startDate < endDate) {
      let nextDate = this.getNextBlockDate(startDate, interval);
      if (nextDate > endDate) {
        nextDate = endDate;
      }

      const minutes = diffInMinutes(nextDate, startDate);

      const blockTasks = this.findBlockTasks(
        {
          startDate,
          endDate: nextDate,
        },
        tasks,
      );
      const { size, minuteSize } = this.getBlockSize({
        blockTasks,
        blockMinutes: minutes,
        taskSizeMinimum,
        baseMinuteSize,
        blockSizeMinimum,
      });

      result.push({
        startTime: getTime(startDate, tz),
        endTime: getTime(nextDate, tz),
        startDate,
        endDate: nextDate,
        isBefore: false,
        isAfter: false,
        minutes,
        size,
        minuteSize,
      });

      startDate = nextDate;
    }

    return result;
  }

  private getNextBlockDate(date: Date, interval: TimeLineBlockInterval): Date {
    if (interval === TimeLineBlockInterval.Hour) {
      return setMinutes(addHours(date, 1), 0);
    }
    if (interval === TimeLineBlockInterval.HourHalf) {
      const mins = getMinutes(date);
      if (mins < 30) {
        return setMinutes(date, 30);
      }
      return setMinutes(addHours(date, 1), 0);
    }
    if (interval === TimeLineBlockInterval.HourQuarter) {
      const mins = getMinutes(date);
      if (mins < 15) {
        return setMinutes(date, 15);
      }
      if (mins < 30) {
        return setMinutes(date, 30);
      }
      if (mins < 45) {
        return setMinutes(date, 45);
      }
      return setMinutes(addHours(date, 1), 0);
    }

    throw new Error(`Unsupported time line block interval "${interval}"`);
  }

  private findBlockTasks(
    block: Pick<BaseTimeLineBlock, 'startDate' | 'endDate'>,
    tasks: TaskForTimeLine[],
  ): TaskForTimeLine[] {
    const { startDate: blockStartDate, endDate: blockEndDate } = block;

    const blockTasks = tasks.filter(
      ({ start: taskStartDate, end: taskEndDate }) =>
        intersectionInMinutes(blockStartDate, blockEndDate, taskStartDate, taskEndDate) > 0,
    );

    return blockTasks;
  }
}

export const timeLineService = new TimeLineService();
