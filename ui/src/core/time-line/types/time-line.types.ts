import { Time, TimeZone } from '@daytab/common';

import type { Day } from '@core/days';
import type { Task } from '@core/tasks';
import type { Minutes, Size } from '@core/types';

export type TaskForTimeLine = Pick<Task, 'start' | 'end'>;

export interface TimeLineGenerateParams {
  /** Required to calculate sizes */
  tasks: TaskForTimeLine[];

  day: Pick<Day, 'date' | 'startTime' | 'endTime'>;

  blockSizeMinimum?: Size;

  taskSizeMinimum?: Size;

  baseMinuteSize?: Size;

  timeZone: TimeZone;

  dayEndTime: Time;

  interval?: TimeLineBlockInterval;
}

export interface TimeLineBlock {
  startTime: Time;
  endTime: Time;
  startDate: Date;
  endDate: Date;
  isBefore: boolean;
  isAfter: boolean;
  minutes: Minutes;
  size: Size;
  minuteSize: Size;
}

export type TimeLine = TimeLineBlock[];

export enum TimeLineBlockInterval {
  Hour = 'HOUR',
  HourHalf = 'HOUR_HALF',
  HourQuarter = 'HOUR_QUARTER',
}

export interface BaseTimeLineBlock {
  startTime: Time;
  endTime: Time;
  startDate: Date;
  endDate: Date;
  minutes: Minutes;
  isBefore: boolean;
  isAfter: boolean;
}
