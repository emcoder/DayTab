import * as t from 'io-ts';

interface ColorBrand {
  readonly Color: unique symbol;
}

const isColor = (value: string): value is t.Branded<string, ColorBrand> => {
  return typeof value === 'string' && value.length > 0;
};

export const ColorType = t.brand(t.string, isColor, 'Color');

export type Color = t.TypeOf<typeof ColorType>;
