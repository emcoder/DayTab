import * as t from 'io-ts';

interface MinutesBrand {
  readonly Minutes: unique symbol;
}

function isMinutes(value: number): value is t.Branded<t.Int, MinutesBrand> {
  return Number.isInteger(value) && value >= 0;
}

export const MinutesType = t.brand(t.Int, isMinutes, 'Minutes');

export type Minutes = t.TypeOf<typeof MinutesType>;
