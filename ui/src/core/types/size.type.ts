import * as t from 'io-ts';

interface SizeBrand {
  readonly Size: unique symbol;
}

const isSize = (n: number): n is t.Branded<t.Int, SizeBrand> => Number.isInteger(n) && n >= 0;

export const SizeType = t.brand(t.Int, isSize, 'Size');

export type Size = t.TypeOf<typeof SizeType>;
