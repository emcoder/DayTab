import { TimeZone, UserId } from '@daytab/common';

export interface User {
  id: UserId;
  username: string;
  timeZone: TimeZone;
  language: string;
}
