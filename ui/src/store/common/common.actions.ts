import { DayDate } from '@daytab/common';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { isSome } from 'fp-ts/lib/Option';

import { logger } from '@common/logger';
import { Day, daysService } from '@core/days';

import { UnauthorizedError } from '../../api/errors';
import { ApiDisplayError } from '../errors';
import { refreshSession, selectCurrentUser } from '../session';
import { refreshTasks } from '../tasks';

import type { AppDispatch, AppState } from '../types';

function getClientErrorMessage(err: Error) {
  if (err instanceof UnauthorizedError) {
    return 'Incorrect username or password';
  }
  return 'Network error, please try again later';
}

export const updateDay = createAsyncThunk(
  'days/update',
  async (params: Day, { rejectWithValue }) => {
    try {
      const result = await daysService.save({
        ...params,
      });

      return result;
    } catch (err) {
      const displayError: ApiDisplayError = {
        message: getClientErrorMessage(<Error>err),
      };
      return rejectWithValue(displayError);
    }
  },
);

export const refreshCurrentDay = createAsyncThunk<
  // Return type of the payload creator
  Day,
  // First argument to the payload creator
  void,
  {
    // Optional fields for defining thunkApi field types
    rejectValue: ApiDisplayError;
    dispatch: AppDispatch;
    state: AppState;
  }
>('common/refreshCurrentDay', async (_: void, { getState, rejectWithValue }) => {
  try {
    const user = selectCurrentUser(getState());
    if (!user) {
      return rejectWithValue({
        message: 'Unauthorized',
      });
    }
    const { id: userId, timeZone } = user;

    const currentDayDate = daysService.findCurrentDayDate(timeZone);
    const dayOption = await daysService.findOne({ userId, date: currentDayDate });
    const day = isSome(dayOption) ? dayOption.value : daysService.initDay(userId, currentDayDate);

    daysService.setCurrentDayDate(currentDayDate);

    return day;
  } catch (err) {
    const displayError: ApiDisplayError = {
      message: `Failed to set current day: ${(<Error>err).message}`,
    };
    return rejectWithValue(displayError);
  }
});

export const selectDay = createAsyncThunk<
  Day,
  DayDate,
  {
    // Optional fields for defining thunkApi field types
    rejectValue: ApiDisplayError;
    dispatch: AppDispatch;
    state: AppState;
  }
>(
  'common/selectDay',
  // eslint-disable-next-line @typescript-eslint/require-await
  async (date: DayDate, { getState, rejectWithValue }) => {
    try {
      const user = selectCurrentUser(getState());
      if (!user) {
        return rejectWithValue({
          message: 'Unauthorized',
        });
      }
      const { id: userId } = user;

      const dayOption = await daysService.findOne({ userId, date });
      const day = isSome(dayOption) ? dayOption.value : daysService.initDay(userId, date);

      daysService.setCurrentDayDate(date);

      return day;
    } catch (err) {
      const displayError: ApiDisplayError = {
        message: 'Failed to select day',
      };
      return rejectWithValue(displayError);
    }
  },
);

export const refreshDayWithTasks = createAsyncThunk<
  true,
  void,
  {
    dispatch: AppDispatch;
  }
>('common/refreshDayWithTasks', async (_: void, { dispatch, rejectWithValue }) => {
  const sessionResult = await dispatch(refreshSession());
  if (refreshSession.rejected.match(sessionResult)) {
    const msg = `Failed to refresh session: ${sessionResult.payload?.message}`;
    logger.error(msg);
    return rejectWithValue(msg);
  }

  const dayResult = await dispatch(refreshCurrentDay());
  if (refreshCurrentDay.rejected.match(dayResult)) {
    const msg = `Failed to refresh current day: ${dayResult.payload?.message}`;
    logger.error(msg);
    return rejectWithValue({
      message: msg,
    });
  }

  const { date } = dayResult.payload;

  const tasksResult = await dispatch(refreshTasks({ date }));
  if (refreshTasks.rejected.match(tasksResult)) {
    const msg = `Failed to refresh tasks: ${tasksResult.payload?.message}`;
    logger.error(msg);
    return rejectWithValue({
      message: msg,
    });
  }

  return true;
});
