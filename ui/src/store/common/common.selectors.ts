import type { AppState } from '../types';

export const selectCurrentDay = (state: AppState) => state.common.currentDay;

export const isRefreshPendingSelector = (state: AppState) => state.common.isRefreshPending;
