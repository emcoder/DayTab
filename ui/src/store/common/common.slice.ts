/* eslint-disable no-param-reassign */
import { UserId } from '@daytab/common';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { getBrowserTimeZone } from '@common/helpers/time-zone.helpers';
import { Day, daysService } from '@core/days';

import { refreshCurrentDay, refreshDayWithTasks, selectDay, updateDay } from './common.actions';
import { CommonState } from './common.types';

export const initialCommonState: CommonState = {
  // FIXME: Get rid of empty day init
  currentDay: daysService.createCurrentDay(<UserId>0, getBrowserTimeZone()),
  isRefreshPending: false,
};

export const commonSlice = createSlice({
  name: 'common',
  initialState: initialCommonState,
  reducers: {
    setCurrentDay: (state, action: PayloadAction<{ day: Day }>) => {
      state.currentDay = action.payload.day;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(updateDay.fulfilled, (state, action) => {
        state.currentDay = action.payload;
      })
      .addCase(refreshCurrentDay.fulfilled, (state, action) => {
        state.currentDay = action.payload;
      })
      .addCase(selectDay.fulfilled, (state, action) => {
        state.currentDay = action.payload;
      })
      .addCase(refreshDayWithTasks.pending, (state) => {
        state.isRefreshPending = true;
      })
      .addCase(refreshDayWithTasks.fulfilled, (state) => {
        state.isRefreshPending = false;
      })
      .addCase(refreshDayWithTasks.rejected, (state) => {
        state.isRefreshPending = false;
      });
  },
});
