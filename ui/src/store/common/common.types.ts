import { Day } from '@core/days';

export interface CommonState {
  currentDay: Day;
  isRefreshPending: boolean;
}
