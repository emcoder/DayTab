import { createAsyncThunk } from '@reduxjs/toolkit';

import { getToken, isExpired, setToken } from '@common/auth';
import { User } from '@core/users';

import { authApi } from '../../api/auth-api';
import { UnauthorizedError } from '../../api/errors';
import { usersClient } from '../../api/users.client';
import { ApiDisplayError } from '../errors';

import type { AppDispatch } from '..';

function getClientErrorMessage(err: Error) {
  if (err instanceof UnauthorizedError) {
    return 'Incorrect username or password';
  }
  return 'Network error, please try again later';
}

export interface SignInParams {
  username: string;
  password: string;
}

export const signIn = createAsyncThunk<
  User,
  SignInParams,
  {
    rejectValue: ApiDisplayError;
  }
>('session/signIn', async (params: SignInParams, { rejectWithValue }) => {
  try {
    const { username, password } = params;
    const { token } = await authApi.signIn(username, password);

    if (!token) {
      throw new Error('Sorry, empty token received. Try again later!');
    }
    setToken(token);

    const user = await usersClient.findCurrentUser();

    return user;
  } catch (err) {
    const displayError: ApiDisplayError = {
      message: getClientErrorMessage(<Error>err),
    };
    return rejectWithValue(displayError);
  }
});

export const signOut = createAsyncThunk<
  void,
  void,
  {
    dispatch: AppDispatch;
  }
>('session/logout', (_: void) => {
  setToken(null);
});

export const refreshSession = createAsyncThunk<
  User,
  void,
  {
    dispatch: AppDispatch;
    rejectValue: ApiDisplayError;
  }
>('session/refresh', async (_: void, { dispatch, rejectWithValue }) => {
  const token = getToken();

  if (!token || isExpired(token)) {
    dispatch(signOut());
    return rejectWithValue({
      message: 'Unauthorized',
    });
  }

  try {
    const user = await usersClient.findCurrentUser();
    return user;
  } catch (err) {
    return rejectWithValue({
      message: getClientErrorMessage(<Error>err),
    });
  }
});
