import { TimeZone } from '@daytab/common';

import { Minutes } from '@core/types';

import { SignInState, UserState } from './session.types';

import type { AppState } from '../types';

export const selectAuthorized = (state: AppState) => state.session.authorized;
export const signInSelector = (state: AppState): SignInState => state.session.signIn;

export const selectCurrentUser = (state: AppState): UserState | null => state.session.user || null;
export const selectDefaultTaskDuration = (_state: AppState): Minutes => <Minutes>60;

export const selectCurrentUserTimeZone = (state: AppState): TimeZone | null =>
  state.session.user?.timeZone || null;
