/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

import { signOut, signIn, refreshSession } from './session.actions';
import { SessionState, SignInStatus } from './session.types';

export const initialSessionState: SessionState = {
  authorized: false,
  user: null,
  signIn: {
    status: null,
    error: null,
  },
};

export const sessionSlice = createSlice({
  name: 'session',
  initialState: initialSessionState,
  reducers: {
    logout(state) {
      state.authorized = false;
      state.user = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(signIn.pending, (state) => {
        state.signIn.status = SignInStatus.Pending;
      })
      .addCase(signIn.fulfilled, (state, action) => {
        state.user = action.payload;
        state.authorized = true;
      })
      .addCase(signIn.rejected, (state, action) => {
        state.signIn.status = SignInStatus.Error;
        state.signIn.error = action.error.message || null;
      })
      .addCase(signOut.fulfilled, (state) => {
        state.authorized = false;
        state.user = null;
        state.signIn.status = null;
        state.signIn.error = null;
      })
      .addCase(refreshSession.fulfilled, (state, action) => {
        state.user = action.payload;
        state.authorized = true;
      })
      .addCase(refreshSession.rejected, (state) => {
        state.authorized = false;
        state.user = null;
      });
  },
});
