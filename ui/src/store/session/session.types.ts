import { TimeZone, UserId } from '@daytab/common';

export interface UserState {
  id: UserId;
  language: string;
  timeZone: TimeZone;
  username: string;
}

export enum SignInStatus {
  Pending = 'pending',
  Success = 'success',
  Error = 'error',
}

export interface SignInState {
  status: SignInStatus | null;
  error: string | null;
}

export interface SessionState {
  authorized: boolean;
  user: UserState | null;
  signIn: SignInState;
}
