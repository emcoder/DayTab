import { TgChatId } from '@daytab/common';
import { createAsyncThunk } from '@reduxjs/toolkit';

import { usersClient } from '@api/users.client';

import type { AppDispatch, AppState } from '../types';

export const userLinkTelegram = createAsyncThunk<
  void,
  { tgChatId: TgChatId },
  {
    state: AppState;
    dispatch: AppDispatch;
    rejectValue: { message: string };
  }
>('users/link-telegram', async (params: { tgChatId: TgChatId }, { rejectWithValue }) => {
  try {
    const { tgChatId } = params;

    await usersClient.userLinkToTelegram(tgChatId);
  } catch (err) {
    rejectWithValue({
      message: (err as Error).message,
    });
  }
});
