import { configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';

import { mainSlice } from '../App/state/main.slice';

import { commonSlice } from './common/common.slice';
import { sessionSlice } from './session/session.slice';
import { tasksSlice } from './tasks/tasks.slice';

const reducer = {
  session: sessionSlice.reducer,
  main: mainSlice.reducer,
  common: commonSlice.reducer,
  tasks: tasksSlice.reducer,
};

export const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});
