import { DayDate } from '@daytab/common';
import { createAsyncThunk } from '@reduxjs/toolkit';

import { UnauthorizedError } from '@common/errors';
import { getDateForDateAndTime, getDayEndDate } from '@common/helpers/date.helpers';
import { getBrowserTimeZone } from '@common/helpers/time-zone.helpers';
import { DEFAULT_DAY_END_TIME } from '@core/days';
import { CreateTaskParams, DeleteTaskParams, tasksService, UpdateTaskParams } from '@core/tasks';

import { ApiDisplayError } from '../errors';

import { getNewTasksState, taskFromState, tasksSorter } from './tasks.helpers';
import { TasksState } from './tasks.types';

import type { AppDispatch, AppState } from '../types';

export const createTask = createAsyncThunk<
  TasksState,
  CreateTaskParams,
  {
    state: AppState;
    dispatch: AppDispatch;
    rejectValue: { message: string };
  }
>('tasks/create', async (params: CreateTaskParams, { getState, rejectWithValue }) => {
  try {
    const state = getState();
    const createdTask = await tasksService.create(params);
    const { currentTasks } = state.tasks;
    const tasks = [...currentTasks.map((task) => taskFromState(task)), createdTask].sort(
      tasksSorter,
    );

    return getNewTasksState(tasks, state);
  } catch (err) {
    return rejectWithValue({
      message: (err as Error).message,
    });
  }
});

export const updateTask = createAsyncThunk<
  TasksState,
  UpdateTaskParams,
  {
    state: AppState;
    dispatch: AppDispatch;
    rejectValue: { message: string };
  }
>('tasks/update', async (params: UpdateTaskParams, { getState, rejectWithValue }) => {
  try {
    const state = getState();
    const tasks = state.tasks.currentTasks.map((task) => taskFromState(task));
    const updatedTask = await tasksService.update(params);
    const taskIndex = tasks.findIndex(({ id }) => id === updatedTask.id);
    if (taskIndex !== -1) {
      tasks[taskIndex] = updatedTask;
    }
    const newTasks = tasks.sort(tasksSorter);

    return getNewTasksState(newTasks, state);
  } catch (err) {
    return rejectWithValue({
      message: (err as Error).message,
    });
  }
});

export interface RefreshTasksParams {
  date: DayDate;
}

export const refreshTasks = createAsyncThunk<
  TasksState,
  RefreshTasksParams,
  {
    dispatch: AppDispatch;
    rejectValue: ApiDisplayError;
    state: AppState;
  }
>('tasks/refresh', async (params: RefreshTasksParams, { getState, rejectWithValue }) => {
  try {
    const state = getState();
    const { user } = state.session;
    if (!user) {
      throw new UnauthorizedError();
    }
    const timeZone = user.timeZone || getBrowserTimeZone();
    const { date } = params;

    const start = getDateForDateAndTime(date, DEFAULT_DAY_END_TIME, timeZone);
    const end = getDayEndDate(date, DEFAULT_DAY_END_TIME, timeZone);

    const tasks = await tasksService.find({
      start,
      end,
    });

    return getNewTasksState(tasks, state);
  } catch (err) {
    return rejectWithValue({
      message: (err as Error).message,
    });
  }
});

export const deleteTask = createAsyncThunk<
  TasksState,
  DeleteTaskParams,
  {
    state: AppState;
    dispatch: AppDispatch;
    rejectValue: { message: string };
  }
>('tasks/delete', async (params: DeleteTaskParams, { getState, rejectWithValue }) => {
  try {
    const state = getState();
    const deletedTask = await tasksService.delete(params);
    const { id: deletedId } = deletedTask;
    const {
      tasks: { currentTasks },
    } = state;

    const tasks = currentTasks.map((task) => taskFromState(task));
    const preserveTasks = tasks.filter(({ id }) => id !== deletedId);

    return getNewTasksState(preserveTasks, state);
  } catch (err) {
    return rejectWithValue({
      message: (err as Error).message,
    });
  }
});
