import { parseISO } from 'date-fns';

import { getBrowserTimeZone } from '@common/helpers/time-zone.helpers';
import { logger } from '@common/logger';
import { DEFAULT_DAY_END_TIME } from '@core/days';
import { Task, tasksDisplayService } from '@core/tasks';
import { TimeLine, timeLineService } from '@core/time-line';

import { TasksState, TaskState } from './tasks.types';

import type { AppState } from '@store/types';

export function taskFromState(taskState: TaskState): Task {
  const { id, name, start, end } = taskState;

  return <Task>{
    id,
    name,
    start: parseISO(start),
    end: parseISO(end),
  };
}

export function tasksSorter({ start: a }: Task, { start: b }: Task): number {
  return Number(a) - Number(b);
}

export function getNewTasksState(tasks: Task[], state: AppState): TasksState {
  logger.debug('Produce tasks state...');
  const { currentDay } = state.common;
  const timeZone = state.session.user?.timeZone || getBrowserTimeZone();

  logger.debug('Generate time line...');
  let timeLine: TimeLine;
  try {
    timeLine = timeLineService.generate({
      day: currentDay,
      tasks,
      timeZone,
      dayEndTime: DEFAULT_DAY_END_TIME,
    });
  } catch (err) {
    logger.error(`Failed to generate time line: ${(<Error>err).message}`);
    throw err;
  }

  logger.debug('Get tasks state according to time line...');
  const tasksState = tasksDisplayService.getTasksState(tasks, timeLine);

  logger.debug('Get time line state...');
  const timeLineState = timeLineService.toState(timeLine);

  return {
    currentTasks: tasksState,
    timeLine: timeLineState,
  };
}
