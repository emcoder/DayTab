import type { AppState } from '../types';

export const selectCurrentTasks = (state: AppState) => state.tasks.currentTasks;

export const selectTimeLine = (state: AppState) => state.tasks.timeLine;
