/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

import { createTask, deleteTask, refreshTasks, updateTask } from './tasks.actions';
import { TasksState } from './tasks.types';

export const initialTasksState: TasksState = {
  currentTasks: [],
  timeLine: [],
};

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState: initialTasksState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(refreshTasks.fulfilled, (state, action) => {
        const {
          payload: { currentTasks, timeLine },
        } = action;
        state.currentTasks = currentTasks;
        state.timeLine = timeLine;
      })
      .addCase(createTask.fulfilled, (state, action) => {
        const {
          payload: { currentTasks, timeLine },
        } = action;
        state.currentTasks = currentTasks;
        state.timeLine = timeLine;
      })
      .addCase(updateTask.fulfilled, (state, action) => {
        const {
          payload: { currentTasks, timeLine },
        } = action;
        state.currentTasks = currentTasks;
        state.timeLine = timeLine;
      })
      .addCase(deleteTask.fulfilled, (state, action) => {
        const {
          payload: { currentTasks, timeLine },
        } = action;
        state.currentTasks = currentTasks;
        state.timeLine = timeLine;
      });
  },
});
