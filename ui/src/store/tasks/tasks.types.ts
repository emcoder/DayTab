import { IsoDate, Time } from '@daytab/common';

import type { Color, Minutes, Size } from '@core/types';

export interface TaskState {
  id: number;
  name: string;
  start: IsoDate;
  end: IsoDate;
  color: Color;
  size: Size;
  offset: Size;
  level: number;
}

export interface TimeLineBlockState {
  startTime: Time;
  endTime: Time;
  startDate: IsoDate;
  endDate: IsoDate;
  isBefore: boolean;
  isAfter: boolean;
  minutes: Minutes;
  size: Size;
  minuteSize: Size;
}

export interface TasksState {
  currentTasks: TaskState[];
  timeLine: TimeLineBlockState[];
}
