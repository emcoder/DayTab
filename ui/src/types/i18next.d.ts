import { defaultNS } from '@common/i18n';

import * as resources from '../../../locale/localization.json';

declare module 'i18next' {
  interface CustomTypeOptions {
    defaultNS: typeof defaultNS;
    resources: typeof resources['en'];
  }
}
